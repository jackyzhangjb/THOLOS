SET NOCOUNT ON
DELETE FROM [dbo].[SurveyProrotypeQuestion];
DELETE FROM [dbo].[SurveyPostTestingQuestion];
DELETE FROM [dbo].[participantCredentials];
DELETE FROM [dbo].[ProfileRangeAnswer];
DELETE FROM [dbo].ProfileSingleInputAnswer;
DELETE FROM [dbo].[ProfileSingleAnswer];
DELETE FROM [dbo].ProfileAnswer;
DELETE FROM [dbo].[ProfileRangeAnswerOption];
DELETE FROM [dbo].[ProfileSingleAnswerOption];
DELETE FROM [dbo].[ProfileAnswerOption];
DELETE FROM [dbo].[ProfileQuestionCondition];
DELETE FROM [dbo].[ProfileQuestion];

DELETE FROM [dbo].[PrototypeRangeAnswer];
DELETE FROM [dbo].PrototypeSingleInputAnswer;
DELETE FROM [dbo].[PrototypeSingleAnswer];

DELETE FROM [dbo].PrototypeAnswer;
DELETE FROM [dbo].[PrototypeRangeAnswerOption];
DELETE FROM [dbo].[PrototypeSingleAnswerOption];
DELETE FROM [dbo].[PrototypeAnswerOption];
DELETE FROM [dbo].[PrototypeQuestionCondition];
DELETE FROM [dbo].[PrototypeQuestion];

DELETE FROM [dbo].[PostTestingRangeAnswer];
DELETE FROM [dbo].PostTestingSingleInputAnswer;
DELETE FROM [dbo].[PostTestingSingleAnswer];

DELETE FROM [dbo].PostTestingAnswer;
DELETE FROM [dbo].[PostTestingRangeAnswerOption];
DELETE FROM [dbo].[PostTestingSingleAnswerOption];
DELETE FROM [dbo].[PostTestingAnswerOption];
DELETE FROM [dbo].[PostTestingQuestionCondition];
DELETE FROM [dbo].[PostTestingQuestion];

DELETE FROM [dbo].[ParticipantRole];
DELETE FROM [dbo].[Participant];
DELETE FROM [dbo].[UserRole];
DELETE FROM [dbo].[Role];
DELETE FROM [dbo].[Users];
DELETE FROM [dbo].[QuickAccessSurvey];
DELETE FROM [dbo].[PrototypeCode];
DELETE FROM PrototypeTest;
DELETE FROM ParticipantSurvey;

DELETE FROM [dbo].[Survey];
SET NOCOUNT OFF