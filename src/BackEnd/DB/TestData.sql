/*
 * @Description: Defines the Test data for Tholos Survey applications
 * @Author: veshu
 * @Version: 1.0
 * @Copyright: Copyright (C) 2015, TopCoder, Inc. All rights reserved.
 */

DELETE FROM [dbo].[SurveyProrotypeQuestion];
DELETE FROM [dbo].[SurveyPostTestingQuestion];
DELETE FROM [dbo].[participantCredentials];
DELETE FROM [dbo].[ProfileRangeAnswer];
DELETE FROM [dbo].ProfileSingleInputAnswer;
DELETE FROM [dbo].[ProfileSingleAnswer];
DELETE FROM [dbo].ProfileAnswer;
DELETE FROM [dbo].[ProfileRangeAnswerOption];
DELETE FROM [dbo].[ProfileSingleAnswerOption];
DELETE FROM [dbo].[ProfileAnswerOption];
DELETE FROM [dbo].[ProfileQuestionCondition];
DELETE FROM [dbo].[ProfileQuestion];

DELETE FROM [dbo].[PrototypeRangeAnswer];
DELETE FROM [dbo].PrototypeSingleInputAnswer;
DELETE FROM [dbo].[PrototypeSingleAnswer];

DELETE FROM [dbo].PrototypeAnswer;
DELETE FROM [dbo].[PrototypeRangeAnswerOption];
DELETE FROM [dbo].[PrototypeSingleAnswerOption];
DELETE FROM [dbo].[PrototypeAnswerOption];
DELETE FROM [dbo].[PrototypeQuestionCondition];
DELETE FROM [dbo].[PrototypeQuestion];

DELETE FROM [dbo].[PostTestingRangeAnswer];
DELETE FROM [dbo].PostTestingSingleInputAnswer;
DELETE FROM [dbo].[PostTestingSingleAnswer];

DELETE FROM [dbo].PostTestingAnswer;
DELETE FROM [dbo].[PostTestingRangeAnswerOption];
DELETE FROM [dbo].[PostTestingSingleAnswerOption];
DELETE FROM [dbo].[PostTestingAnswerOption];
DELETE FROM [dbo].[PostTestingQuestionCondition];
DELETE FROM [dbo].[PostTestingQuestion];

DELETE FROM [dbo].[ParticipantRole];
DELETE FROM [dbo].[Participant];
DELETE FROM [dbo].[UserRole];
DELETE FROM [dbo].[Role];
DELETE FROM [dbo].[Users];
DELETE FROM [dbo].[QuickAccessSurvey];
DELETE FROM [dbo].[PrototypeCode];
DELETE FROM PrototypeTest;
DELETE FROM ParticipantSurvey;

DELETE FROM [dbo].[Survey];


INSERT INTO [dbo].[Role](Id, Name) VALUES(1,'Manager');
INSERT INTO [dbo].[Role](Id, Name) VALUES(2,'Participant');

INSERT INTO [dbo].[Users](Id, Name, Username, [Password], UserpicPath)VALUES(1,'Topcoder Manager','topcoder','5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8','i/thumn-user.jpg');
INSERT INTO [dbo].[Users](Id, Name, Username, [Password], UserpicPath)VALUES(2,'Second user','seconduser','5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8','i/thumn-user.jpg');

INSERT INTO [dbo].[UserRole](UserId, RoleId)VALUES(1,1);
INSERT INTO [dbo].[UserRole](UserId, RoleId)VALUES(2,1);

SET IDENTITY_INSERT [dbo].[Participant] ON;
INSERT INTO [dbo].[Participant](Id,Username, SurveyId, BirthDate, Weight, HeightFeet, HeightInches)VALUES(1, 'participant1',1,'1992-07-24', 67, 5, 11)
INSERT INTO [dbo].[Participant](Id,Username, SurveyId, BirthDate, Weight, HeightFeet, HeightInches)VALUES(2, 'participant2',1,'1992-07-24', 67, 5, 11)
SET IDENTITY_INSERT [dbo].[Participant] OFF;

INSERT INTO [dbo].[ParticipantRole](ParticipantId, RoleId) VALUES(1,2);
INSERT INTO [dbo].[ParticipantRole](ParticipantId, RoleId) VALUES(2,2);

SET IDENTITY_INSERT [dbo].[Survey] ON;
INSERT INTO [dbo].[Survey](Id,Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy)VALUES(1,'Survey1','CellName1',3,'Published',0,0,1);
INSERT INTO [dbo].[Survey](Id,Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy)VALUES(2,'Survey2','CellName2',3,'Draft',0,1,1);
INSERT INTO [dbo].[Survey](Id,Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy)VALUES(3,'Survey3','CellName3',3,'Draft',0,1,1);
SET IDENTITY_INSERT [dbo].[Survey] OFF;

SET IDENTITY_INSERT [dbo].[participantCredentials] ON;
INSERT INTO [dbo].[participantCredentials](Id, surveyid,Username, [password],used)VALUES(1,1,'participant1','3EJmN7DbtlAJXiDNwLPKZQ==',1);
INSERT INTO [dbo].[participantCredentials](Id, surveyid,Username, [password],used)VALUES(2,1,'participant2','qCoFhOshU+3B4TSSD+PzBQ==',1);
INSERT INTO [dbo].[participantCredentials](Id, surveyid,Username, [password],used)VALUES(3,1,'participant3','dlya20FTyoGxt7NGHC2XzA==',0);
INSERT INTO [dbo].[participantCredentials](Id, surveyid,Username, [password],used)VALUES(4,1,'participant4','Totz77kr0oVRA78hPcwl7Q==',0);
SET IDENTITY_INSERT [dbo].[participantCredentials] OFF;

-- Profile questions  and answer options
INSERT INTO [dbo].[ProfileQuestion](Id, [Text], Conditional, QuestionType) VALUES(1, 'Question1',0,'SingleAnswerQuestion');
INSERT INTO [dbo].[ProfileQuestion](Id, [Text], Conditional, QuestionType) VALUES(2, 'Question2',1,'SingleAnswerQuestion');
INSERT INTO [dbo].[ProfileQuestion](Id, [Text], Conditional, QuestionType) VALUES(3, 'Question3',0,'RangeQuestion');
INSERT INTO [dbo].[ProfileQuestion](Id, [Text], Conditional, QuestionType) VALUES(4, 'Question4',0,'SingleAnswerQuestion');


INSERT INTO dbo.ProfileQuestionCondition(Id, QuestionId, ConditionalOnQuestionId, ConditionalOnAnswerId)VALUES(1,2,4,1);

INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(1,1,'SingleAnswer');
INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(2,1,'SingleAnswer');
INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(3,1,'SingleAnswer');
INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(4,1,'SingleAnswer');

INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(5,2,'SingleAnswer');
INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(6,2,'SingleAnswer');

INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(7,3,'RangeAnswer');
INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(8,3,'RangeAnswer');
INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(9,3,'RangeAnswer');

INSERT INTO dbo.ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(10,4,'SingleAnswerInput');

INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(1, 1, '4',null);
INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(2, 2, '6',null);
INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(3, 3, '7',null);
INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(4, 4, '8',null);
INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(5, 1, 'Yes',null);
INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(6, 2, 'No',null);

INSERT INTO dbo.ProfileRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(7,'Sleep At', 'Wake at',10,6,1);
INSERT INTO dbo.ProfileRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(8,'Sleep At', 'Wake at',10,6,2);
INSERT INTO dbo.ProfileRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(9,'Sleep At', 'Wake at',10,6,3);

INSERT INTO dbo.ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(10, 1, 'Explain Why?',null);

-- prototype questions  and answer options
INSERT INTO [dbo].[PrototypeQuestion](Id, [Text], Conditional, QuestionType) VALUES(1, 'Question1',0,'SingleAnswerQuestion');
INSERT INTO [dbo].[PrototypeQuestion](Id, [Text], Conditional, QuestionType) VALUES(2, 'Question2',1,'SingleAnswerQuestion');
INSERT INTO [dbo].[PrototypeQuestion](Id, [Text], Conditional, QuestionType) VALUES(3, 'Question3',0,'RangeQuestion');
INSERT INTO [dbo].[PrototypeQuestion](Id, [Text], Conditional, QuestionType) VALUES(4, 'Question4',0,'SingleAnswerQuestion');
																					   
INSERT INTO dbo.PrototypeQuestionCondition(Id, QuestionId, ConditionalOnQuestionId, ConditionalOnAnswerId)VALUES(1,2,4,1);

INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(1,1,'SingleAnswer');
INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(2,1,'SingleAnswer');
INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(3,1,'SingleAnswer');
INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(4,1,'SingleAnswer');

INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(5,2,'SingleAnswer');
INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(6,2,'SingleAnswer');

INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(7,3,'RangeAnswer');
INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(8,3,'RangeAnswer');
INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(9,3,'RangeAnswer');

INSERT INTO dbo.PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(10,4,'SingleAnswerInput');

INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(1, 1, '4',null);
INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(2, 2, '6',null);
INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(3, 3, '7',null);
INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(4, 4, '8',null);
INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(5, 1, 'Yes',null);
INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(6, 2, 'No',null);

INSERT INTO dbo.PrototypeRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(7,'Sleep At', 'Wake at',10,6,1);
INSERT INTO dbo.PrototypeRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(8,'Sleep At', 'Wake at',10,6,2);
INSERT INTO dbo.PrototypeRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(9,'Sleep At', 'Wake at',10,6,3);

INSERT INTO dbo.PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(10, 1, 'Explain Why?',null);

-- PostTesting questions and answer options
INSERT INTO [dbo].[PostTestingQuestion](Id, [Text], Conditional, QuestionType) VALUES(1, 'Question1',0,'SingleAnswerQuestion');
INSERT INTO [dbo].[PostTestingQuestion](Id, [Text], Conditional, QuestionType) VALUES(2, 'Question2',1,'SingleAnswerQuestion');
INSERT INTO [dbo].[PostTestingQuestion](Id, [Text], Conditional, QuestionType) VALUES(3, 'Question3',0,'RangeQuestion');
INSERT INTO [dbo].[PostTestingQuestion](Id, [Text], Conditional, QuestionType) VALUES(4, 'Question4',0,'SingleAnswerQuestion');

INSERT INTO dbo.PostTestingQuestionCondition(Id, QuestionId, ConditionalOnQuestionId, ConditionalOnAnswerId)VALUES(1,2,4,1);

INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(1,1,'SingleAnswer');
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(2,1,'SingleAnswer');
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(3,1,'SingleAnswer');
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(4,1,'SingleAnswer');

INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(5,2,'SingleAnswer');
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(6,2,'SingleAnswer');

INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(7,3,'RangeAnswer');
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(8,3,'RangeAnswer');
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(9,3,'RangeAnswer');
				
INSERT INTO dbo.PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(10,4,'SingleAnswerInput');
				
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(1, 1, '4',null);
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(2, 2, '6',null);
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(3, 3, '7',null);
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(4, 4, '8',null);
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(5, 1, 'Yes',null);
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(6, 2, 'No',null);
				
INSERT INTO dbo.PostTestingRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(7,'Sleep At', 'Wake at',10,6,1);
INSERT INTO dbo.PostTestingRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(8,'Sleep At', 'Wake at',10,6,2);
INSERT INTO dbo.PostTestingRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(9,'Sleep At', 'Wake at',10,6,3);
				
INSERT INTO dbo.PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(10, 1, 'Explain Why?',null);

-- Profile ANswers
SET IDENTITY_INSERT [dbo].[ProfileAnswer] ON;
INSERT INTO [dbo].[ProfileAnswer](Id,QuestionId,AnswerType,ParticipantId) VALUES (1,1,'SingleAnswer'	,1)
INSERT INTO [dbo].[ProfileAnswer](Id,QuestionId,AnswerType,ParticipantId) VALUES (2,3,'RangeAnswer'		,1)
INSERT INTO [dbo].[ProfileAnswer](Id,QuestionId,AnswerType,ParticipantId) VALUES (3,2,'SingleAnswerInput',1)
SET IDENTITY_INSERT [dbo].[ProfileAnswer] OFF;

INSERT INTO [dbo].[ProfileSingleAnswer](Id,AnswerOptionId) VALUES (1, 1);
INSERT INTO [dbo].[ProfileSingleAnswer](Id,AnswerOptionId) VALUES (3, 2);

INSERT INTO [dbo].[ProfileSingleInputAnswer](Id,Input) VALUES (3,11);
INSERT INTO [dbo].[ProfileRangeAnswer](Id,Value) VALUES (2,4);

SET IDENTITY_INSERT [dbo].[QuickAccessSurvey] ON;
INSERT INTO	[dbo].[QuickAccessSurvey](Id, UserId, SurveyId, Position) VALUES(1,1,1,1);
INSERT INTO	[dbo].[QuickAccessSurvey](Id, UserId, SurveyId, Position) VALUES(2,1,2,2);
SET IDENTITY_INSERT [dbo].[QuickAccessSurvey] OFF;

SET IDENTITY_INSERT [dbo].[PrototypeCode] ON;
INSERT INTO [dbo].[PrototypeCode](Id, Code, PrototypesPerCode,surveyId)VALUES(1,'Code1',3,1);
INSERT INTO [dbo].[PrototypeCode](Id, Code, PrototypesPerCode,surveyId)VALUES(2,'Code2',3,1);
INSERT INTO [dbo].[PrototypeCode](Id, Code, PrototypesPerCode,surveyId)VALUES(3,'Code3',3,1);
INSERT INTO [dbo].[PrototypeCode](Id, Code, PrototypesPerCode,surveyId)VALUES(4,'Code1',3,2);
SET IDENTITY_INSERT [dbo].[PrototypeCode] OFF;

SET IDENTITY_INSERT [dbo].[SurveyProrotypeQuestion] ON;
INSERT INTO [dbo].[SurveyProrotypeQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(1,1,1,1);
INSERT INTO [dbo].[SurveyProrotypeQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(2,1,2,2);
INSERT INTO [dbo].[SurveyProrotypeQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(3,1,3,3);
INSERT INTO [dbo].[SurveyProrotypeQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(4,2,1,1);
SET IDENTITY_INSERT [dbo].[SurveyProrotypeQuestion] OFF;

SET IDENTITY_INSERT [dbo].[SurveyPostTestingQuestion] ON;
INSERT INTO [dbo].[SurveyPostTestingQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(1,1,1,1);
INSERT INTO [dbo].[SurveyPostTestingQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(2,1,2,2);
INSERT INTO [dbo].[SurveyPostTestingQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(3,1,3,3);
INSERT INTO [dbo].[SurveyPostTestingQuestion](Id, surveyId, QuestionId,QuestionNumber)VALUES(4,2,1,1);
SET IDENTITY_INSERT [dbo].[SurveyPostTestingQuestion] OFF;

INSERT INTO ParticipantSurvey(Id, ParticipantId, SurveyId, CurrentPrototypeTestId)VALUES(1,1,1,1)
INSERT INTO ParticipantSurvey(Id, ParticipantId, SurveyId, CurrentPrototypeTestId)VALUES(2,2,1,1)

SET IDENTITY_INSERT [dbo].PrototypeTest ON;
INSERT INTO PrototypeTest(Id,ParticipantSurveyId,PrototypeCode,Iteration,beforeWeight,BeforePhotoLocalPath,BeforePhotoUri)
VALUES(1,1,'code1',1,12.4,'E:/tc/Csharp/tholos/sub - Copy/submission/Src/Tholos.Host/uploadFiles/download.jpg','http://localhost/tholosapi/uploadFiles/download.jpg');
SET IDENTITY_INSERT [dbo].PrototypeTest OFF;

SET IDENTITY_INSERT [dbo].[PrototypeAnswer] ON;
INSERT INTO [dbo].[PrototypeAnswer](Id,QuestionId,AnswerType,PrototypeTestId) VALUES (1,1,'SingleAnswer'	,1)
INSERT INTO [dbo].[PrototypeAnswer](Id,QuestionId,AnswerType,PrototypeTestId) VALUES (2,3,'RangeAnswer'		,1)
INSERT INTO [dbo].[PrototypeAnswer](Id,QuestionId,AnswerType,PrototypeTestId) VALUES (3,2,'SingleAnswerInput',1)
SET IDENTITY_INSERT [dbo].[PrototypeAnswer] OFF;

INSERT INTO [dbo].[PrototypeSingleAnswer](Id,AnswerOptionId) VALUES (1, 1);
INSERT INTO [dbo].[PrototypeSingleAnswer](Id,AnswerOptionId) VALUES (3, 2);

INSERT INTO [dbo].[PrototypeSingleInputAnswer](Id,Input) VALUES (3,11);
INSERT INTO [dbo].[PrototypeRangeAnswer](Id,Value) VALUES (2,4);