﻿//
// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
//

using System;
using System.ServiceModel;
using Microsoft.Practices.Unity;

namespace Tholos.Host
{
    ///  <summary>
    ///  Custom ServiceHost class that can pass a Unity container instance
    ///  into the WCF infrastructure.
    /// </summary>
    ///
    /// <threadsafety>
    ///  This class is immutable (assuming dependencies are not injected more than once).
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class UnityServiceHost : ServiceHost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnityServiceHost"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="baseAddresses">The base addresses.</param>
        /// <exception cref="ArgumentNullException">If the container is null.</exception>
        public UnityServiceHost(IUnityContainer container,
            Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            foreach (var cd in ImplementedContracts.Values)
            {

                cd.Behaviors.Add(new UnityInstanceProvider(container));

            }
        }
    }
}