/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Services;

namespace Tholos.Tests
{
    /// <summary>
    /// Unit Tests for <see cref="TholosException"/> class.
    /// </summary>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class TholosExceptionUnitTest
    {
        /// <summary>
        /// Message string for test.
        /// </summary>
        private const string Message = "message";

        /// <summary>
        /// Exception instance for test.
        /// </summary>
        private Exception cause = new Exception("innerException");

        /// <summary>
        /// <para>Tests <see cref="TholosException()"/> constructor and inheritance.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtor()
        {
            var instance = new TholosException();
            Assert.AreEqual(typeof(ApplicationException), instance.GetType().BaseType,
                "The class should inherit from ApplicationException.");
        }

        /// <summary>
        /// <para>Tests <see cref="TholosException(string)"/> constructor
        /// by passing a null reference.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorMessageNull()
        {
            new TholosException(null);
        }

        /// <summary>
        /// <para>Tests <see cref="TholosException(string)"/> constructor
        /// by passing an error message.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorMessageValid()
        {
            Exception e = new TholosException(Message);
            Assert.AreEqual(Message, e.Message, "e.Message should be equal to message.");
        }

        /// <summary>
        /// <para>Tests <see cref="TholosException(string, Exception)"/> constructor
        /// by passing null references.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorMessageInnerNull1()
        {
            new TholosException(null, null);
        }

        /// <summary>
        /// <para>Test <see cref="TholosException(string, Exception)"/> constructor
        /// by passing an error message and a null reference.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorMessageInnerNull2()
        {
            Exception e = new TholosException(Message, null);
            Assert.AreEqual(Message, e.Message, "e.Message should be equal to message.");
        }

        /// <summary>
        /// <para>Test <see cref="TholosException(string, Exception)"/> constructor
        /// by passing a null reference and an inner exception.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorMessageInnerNull3()
        {
            Exception e = new TholosException(null, cause);
            Assert.AreEqual(cause, e.InnerException, "e.InnerException should be equal to cause.");
        }

        /// <summary>
        /// <para>Tests <see cref="TholosException(string, Exception)"/> constructor
        /// by passing an error message and an inner exception.</para>
        ///
        /// <para>Should be correct.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorMessageInnerValid()
        {
            Exception e = new TholosException(Message, cause);
            Assert.AreEqual(Message, e.Message, "e.Message should be equal to message.");
            Assert.AreEqual(cause, e.InnerException, "e.InnerException should be equal to cause.");
        }

        /// <summary>
        /// <para>Tests <see cref="TholosException(SerializationInfo, StreamingContext)"/> constructor.</para>
        ///
        /// <para>Deserialized instance should have the same property value as it was before serialization.</para>
        /// </summary>
        [TestMethod]
        public void TestCtorInfoContext()
        {
            // Stream for serialization.
            using (Stream stream = new MemoryStream())
            {
                // serialize the instance
                TholosException serial = new TholosException(Message, cause);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, serial);

                // deserialize the instance
                stream.Seek(0, SeekOrigin.Begin);
                TholosException deserial =
                    formatter.Deserialize(stream) as TholosException;

                // verify the instance
                Assert.IsFalse(serial == deserial, "Instance not deserialized.");
                Assert.AreEqual(serial.Message, deserial.Message, "Message mismatches.");
                Assert.AreEqual(serial.InnerException.Message, deserial.InnerException.Message,
                    "InnerException mismatches.");
            }
        }
    }
}
