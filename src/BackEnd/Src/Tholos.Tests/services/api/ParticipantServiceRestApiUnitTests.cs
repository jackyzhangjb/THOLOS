﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;
using Tholos.Tests.services.api.proxy;

namespace Tholos.Tests.services.api
{
    /// <summary>
    /// <para>
    /// Unit tests for <see cref="ParticipantService"/> class.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Updated tests for CreateParticipant method.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class ParticipantServiceRestApiUnitTests : BasePersistenceServiceUnitTest<ParticipantService>
    {
        /// <summary>
        /// Represents the <see cref="SecurityServiceRestApiProxy"/> instance used for REST API tests.
        /// </summary>
        private readonly IParticipantService _restApiProxy = new ParticipantServiceRestApiProxy();

        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BasePersistenceService), _instance.GetType().BaseType,
                "The class should inherit from BasePersistenceService.");
            Assert.IsTrue(_instance is IParticipantService,
                "The class should implement IParticipantService.");
        }

        #region CreateParticipant tests
        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy1()
        {
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = 7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 89,
                ProfileAnswers = TestHelper.GetAnswersList()
            };
            var result = _restApiProxy.CreateParticipant(entity);

            TestHelper.AssertDatabaseRecordCount("Participant", new Dictionary<string, object>
            {
                {"id", result}
            }, 1);
            entity.Id = result;
            var createdInput = _restApiProxy.GetParticipantById(result.ToString());
            TestHelper.AreEqual(entity, createdInput);
        }


        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check null input
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy3()
        {
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.CreateParticipant(null));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// without answers
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy4()
        {
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = 7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 89,
                ProfileAnswers = new List<Answer>()
            };
            var result = _restApiProxy.CreateParticipant(entity);

            TestHelper.AssertDatabaseRecordCount("Participant", new Dictionary<string, object>
            {
                {"id", result}
            }, 1);
            entity.Id = result;
            var createdInput = _restApiProxy.GetParticipantById(result.ToString());
            TestHelper.AreEqual(entity, createdInput);
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation null username
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy5()
        {
            var entity = new Participant
            {
                HeightFeet = 7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                Username = null,
                Weight = 89,
                ProfileAnswers = new List<Answer>()
            };

            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation empty username
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy2()
        {
            var entity = new Participant
            {
                HeightFeet = 7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                Username = "",
                Weight = 89,
                ProfileAnswers = new List<Answer>()
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation negative Weight
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy6()
        {
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = 7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = -10,
                ProfileAnswers = new List<Answer>()
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation negative HeightFeet
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy7()
        {
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = -7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 10,
                ProfileAnswers = new List<Answer>()
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation wrong answer with no answertype.
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy8()
        {
            var answers = new List<Answer>();
            answers.Add(new SingleAnswer { QuestionId = 1, AnswerOptionId = 2 });
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = -7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 10,
                ProfileAnswers = answers
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation wrong answer with negative questionid .
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy9()
        {
            var answers = new List<Answer>();
            answers.Add(new SingleAnswer
            {
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = -10,
                AnswerOptionId = 2
            });
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = -7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 10,
                ProfileAnswers = answers
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation wrong answer with not existing questionid .
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy10()
        {
            var answers = new List<Answer>();
            answers.Add(new SingleAnswer
            {
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = 200,
                AnswerOptionId = 2
            });
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = -7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 10,
                ProfileAnswers = answers
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }

        /// <summary>
        /// Accuracy test of <c>CreateParticipant</c> method,
        /// check validation wrong answer with not existing AnswerOptionId .
        /// </summary>
        [TestMethod]
        public void TestCreateParticipantAccuracy11()
        {
            var answers = new List<Answer>();
            answers.Add(new SingleAnswer
            {
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = 1,
                AnswerOptionId = 200
            });
            var entity = new Participant
            {
                Username = "participant3",
                HeightFeet = -7,
                BirthDate = new DateTime(2000, 1, 1),
                HeightInches = 1,
                SurveyId = 1,
                Weight = 10,
                ProfileAnswers = answers
            };

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateParticipant(entity));
        }
        #endregion

        #region Get participant by id tests
        /// <summary>
        /// Accuracy test of <c>GetParticipantById</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByIdAccuracy1()
        {
            var result = _restApiProxy.GetParticipantById("1");

            Assert.AreEqual(1, result.Id, "Id is wrong");
            Assert.AreEqual(1, result.SurveyId, "SurveyId is wrong");
            Assert.AreEqual("participant1", result.Username, "Username is wrong");
            Assert.AreEqual(67, result.Weight, "Weight is wrong");
            Assert.AreEqual(11, result.HeightInches, "HeightInches is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantById</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByIdAccuracy2()
        {
            // arrange
            string id = "6";

            // act
            var result = _restApiProxy.GetParticipantById(id);

            // assert
            Assert.IsNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantById</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByIdAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetParticipantById(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantById</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByIdAccuracy5()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetParticipantById(id));
        }
        #endregion

        #region Get participant by username tests

        /// <summary>
        /// Accuracy test of <c>GetParticipantByUserName</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByUsernameAccuracy1()
        {
            var username = "participant2";
            var result = _restApiProxy.GetParticipantByUserName(username);

            Assert.IsNotNull(result, "participant should be null is wrong");
            Assert.AreEqual(2, result.Id, "Id is wrong");
            Assert.AreEqual(1, result.SurveyId, "SurveyId is wrong");
            Assert.AreEqual("participant2", result.Username, "Name is wrong");
            Assert.AreEqual(67, result.Weight, "Weight is wrong");
            Assert.AreEqual(11, result.HeightInches, "HeightInches is wrong");
            Assert.IsNotNull(result.ProfileAnswers, "ProfileAnswers should not be null");

            Assert.AreEqual(0, result.ProfileAnswers.Count, "profile answer must be zero");
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantByUserName</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByUsernameAccuracyWithAnswer()
        {
            var username = "participant1";
            var result = _restApiProxy.GetParticipantByUserName(username);

            Assert.IsNotNull(result, "participant should not be null is wrong");
            Assert.AreEqual(1, result.Id, "Id is wrong");
            Assert.AreEqual(1, result.SurveyId, "SurveyId is wrong");
            Assert.AreEqual("participant1", result.Username, "Name is wrong");
            Assert.AreEqual(67, result.Weight, "Weight is wrong");
            Assert.AreEqual(11, result.HeightInches, "HeightInches is wrong");


            TestHelper.VerifyAnswer(result.ProfileAnswers);
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantByUserName</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByUsernameAccuracy2()
        {
            // arrange
            var username = "DoesnotExist";

            //act
            var result = _restApiProxy.GetParticipantByUserName(username);

            // assert
            Assert.IsNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantByUserName</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByUsernameAccuracy3()
        {
            // arrange
            string username = "";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetParticipantByUserName(username));
        }


        /// <summary>
        /// Accuracy test of <c>GetParticipantByUserName</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantByUsernameAccuracy5()
        {
            // arrange
            string username = null;
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.GetParticipantByUserName(username));
        }
        #endregion

        #region Get all profile questions tests

        /// <summary>
        /// Accuracy test of <c>GetAllProfileQuestions</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetAllProfileQuestionsAccuracy()
        {
            var result = _restApiProxy.GetAllProfileQuestions();

            Assert.IsNotNull(result, "questions should not be null");
            Assert.AreEqual(4, result.Count, "Number of questions is wrong");

            TestHelper.VerifyAnswerOption(result);
        }

        #endregion
    }
}
