﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System.Collections.Generic;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services.api.proxy
{
    /// <summary>
    /// Represents proxy class for hosted WCF RESTful <see cref="ParticipantService"/> service.
    /// </summary>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public class ParticipantServiceRestApiProxy : RestApiProxyBase<ParticipantService>, IParticipantService
    {
        /// <summary>
        /// Creates the participant.
        /// </summary>
        /// <param name="participant">The participant.</param>
        /// <returns>The id of created participant.</returns>
        public long CreateParticipant(Participant participant)
        {
            return GetResponse<long>("/Participants", "POST", participant);
        }

        /// <summary>
        /// Gets the participant by id
        /// </summary>
        /// <param name="participantId">The particiant id.</param>
        /// <returns>The participant.</returns>
        public Participant GetParticipantById(string participantId)
        {
            if (participantId != null)
            {
                return GetResponse<Participant>("/Participants/" + participantId, "GET");
            }
            else
            {
                return GetResponse<Participant>("/Participants/", "GET");
            }
        }

        /// <summary>
        /// Gets the participant by username
        /// </summary>
        /// <param name="username">The particiant username.</param>
        /// <returns>The participant.</returns>
        public Participant GetParticipantByUserName(string username)
        {
            if (username != null)
            {
                return GetResponse<Participant>("/Participants/params?username=" + username, "GET");
            }
            else
            {
                return GetResponse<Participant>("/Participants/params", "GET");
            }
        }

        /// <summary>
        /// Gets all profile questions.
        /// </summary>
        /// <returns>All profile questions.</returns>
        public IList<Question> GetAllProfileQuestions()
        {
            return GetResponse<IList<Question>>("/ProfileQuestions", "GET");
        }
    }
}
