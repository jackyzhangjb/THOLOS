﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System.Collections.Generic;
using System.IO;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services.api.proxy
{
    /// <summary>
    /// Represents proxy class for hosted WCF RESTful <see cref="SurveyService"/> service.
    /// </summary>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public class SurveyServiceRestApiProxy : RestApiProxyBase<SurveyService>, ISurveyService
    {
        /// <summary>
        /// Gets all Prototype questions.
        /// </summary>
        /// <returns>All Prototype questions.</returns>
        public IList<Question> GetAllPrototypeQuestions()
        {
            return GetResponse<IList<Question>>("/PrototypeQuestions", "GET");
        }

        /// <summary>
        /// Gets all PostTesting questions.
        /// </summary>
        /// <returns>All PostTesting questions.</returns>
        public IList<Question> GetAllPostTestingQuestions()
        {
            return GetResponse<IList<Question>>("/PostTestingQuestions", "GET");
        }

        /// <summary>
        /// Creates the survey.
        /// </summary>
        /// <param name="survey">The survey.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The id of created survey.</returns>
        public long CreateSurvey(Survey survey, long userId)
        {
            return GetResponse<long>("/Surveys/Create?userId=" + userId, "POST", survey);
        }

        /// <summary>
        /// publish survey.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        public void PublishSurvey(string surveyId)
        {
            SendRequest("/Surveys/" + surveyId + "/Publish", "PUT");
        }

        /// <summary>
        /// Updates survey.
        /// </summary>
        /// <param name="survey">The survey.</param>
        public void UpdateSurvey(Survey survey)
        {
            SendRequest("/Surveys", "PUT", survey);
        }

        /// <summary>
        /// publish survey.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <returns>The survey.</returns>
        public Survey GetSurvey(string surveyId)
        {
            return GetResponse<Survey>("/Surveys/" + surveyId, "GET");
        }

        /// <summary>
        /// Updates survey status.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="status">The status to update.</param>
        public void UpdateSurveyStatus(string surveyId, SurveyStatus status)
        {
            SendRequest("/Surveys/" + surveyId + "/UpdateSurvey?status=" + status, "PUT");
        }

        /// <summary>
        /// serches survey.
        /// </summary>
        /// <param name="name">The survey name.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>The survey search result.</returns>
        public SurveySearchResult SearchSurvey(string name, SearchFilter filter)
        {
            return GetResponse<SurveySearchResult>("/Surveys/search?name=" + name, "POST", filter);
        }

        /// <summary>
        /// Gets all survey.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>The all survey search result.</returns>       
        public AllSurveysSearchResult GetAllSurveys(SearchFilter filter)
        {
            return GetResponse<AllSurveysSearchResult>("Surveys/GetAll?all=true", "POST", filter);
        }

        /// <summary>
        /// Gets user survey status.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>The survey status and count dictionary.</returns>    
        public IDictionary<SurveyStatus, int> GetUserSurveyStatus(string userId)
        {
            return GetResponse<IDictionary<SurveyStatus, int>>("/Users/" + userId + "/SurveyStatus", "GET");
        }

        /// <summary>
        /// Gets quick access surveys.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>The position and survey of quick  access survyes.</returns>  
        public IList<QuickAccessSurvey> GetQuickAccessSurveys(string userId)
        {
            return GetResponse<IList<QuickAccessSurvey>>("/Users/" + userId + "/QuickAccessSurveys", "GET");
        }

        /// <summary>
        /// Gets quick access surveys.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="surveys">The surveyIds and order dictionary.</param>
        public void UpdateQuickAccessSurveys(string userId, IList<QuickAccessSurvey> surveys)
        {
            SendRequest("/Users/" + userId + "/QuickAccessSurveys", "PUT", surveys);
        }

        /// <summary>
        /// Exports the participant credentials.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <returns>The output stream.</returns>
        public Stream ExportParticipantCredentials(string surveyId)
        {
            return GetResponse<Stream>("/Surveys/" + surveyId + "/ExportParticipantCredentials", "GET");
        }

        /// <summary>
        /// Exports the survey results.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <returns>The output stream.</returns>
        public Stream ExportSurveyResults(string surveyId)
        {
            return GetResponse<Stream>("/Surveys/" + surveyId + "/ExportSurveyResults", "GET");
        }

        /// <summary>
        /// Exports the product images.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <returns>The output stream.</returns>
        public Stream ExportProductImages(string surveyId)
        {
            return GetResponse<Stream>("/Surveys/" + surveyId + "/ExportProductImages", "GET");
        }
    }
}
