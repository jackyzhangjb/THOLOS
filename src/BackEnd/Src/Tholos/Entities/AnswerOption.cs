/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents answer option.
    /// </summary>
    ///  
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [KnownType(typeof(SingleAnswerOption))]
    [KnownType(typeof(RangeAnswerOption))]
    [KnownType(typeof(SingleAnswerInputOption))]
    [DataContract]
    public class AnswerOption : IdentifiableEntity
    {

        /// <summary>
        /// Gets or sets the question id.
        /// </summary>
        /// <value>The question id.</value>
        [DataMember]
        [Required]
        public long QuestionId
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember]
        [Required]
        public AnswerType Type
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="AnswerOption"/> class.
        /// </para>
        /// </summary>
        public AnswerOption()
        {
        }
    }
}

