/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents question type.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This enum is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="EnumMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This enum is always thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public enum QuestionType
    {
        /// <summary>
        /// <para>
        /// Represents single answer.
        /// </para>
        /// </summary>
        [EnumMember]
        SingleAnswerQuestion,

        /// <summary>
        /// <para>
        /// Represents range question.
        /// </para>
        /// </summary>
        [EnumMember]
        RangeQuestion
    }
}

