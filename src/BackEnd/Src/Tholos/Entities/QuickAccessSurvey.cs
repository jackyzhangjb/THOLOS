﻿/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents QuickAccessSurvey.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class QuickAccessSurvey
    {
        /// <summary>
        /// Gets or sets position.
        /// </summary>
        /// <value>The position.</value>
        [DataMember]
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets Survey id.
        /// </summary>
        /// <value>The Survey id.</value>
        [DataMember]
        public long SurveyId { get; set; }

        /// <summary>
        /// Gets or sets Survey name.
        /// </summary>
        /// <value>The Survey name.</value>
        [DataMember]
        public string SurveyName { get; set; }

        /// <summary>
        /// Gets or sets Survey Cell name.
        /// </summary>
        /// <value>The Survey Cell name.</value>
        [DataMember]
        public string CellName { get; set; }

        /// <summary>
        /// Initializes new instance of the <see cref="QuickAccessSurvey"/> class.
        /// </summary>
        public QuickAccessSurvey()
        {

        }
    }
}
