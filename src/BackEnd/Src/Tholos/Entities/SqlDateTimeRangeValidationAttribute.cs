﻿/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;

namespace Tholos.Entities
{
    /// <summary>
    /// This class provides the validation for <see cref="DateTime" /> properties the need
    /// to save into database.
    /// </summary>
    /// 
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class SqlDateTimeRangeValidationAttribute : ValidationAttribute
    {
        /// <summary>
        /// Represents the min date time.
        /// </summary>
        /// <value>The min date time</value>
        private static readonly DateTime MinValue = SqlDateTime.MinValue.Value;

        /// <summary>
        /// Represents the max date time.
        /// </summary>
        /// <value>The max date time</value>
        private static readonly DateTime MaxValue = SqlDateTime.MaxValue.Value;

        /// <summary>
        /// Validate the property.
        /// </summary>
        /// <param name="value">
        /// The property value
        /// </param>
        /// <returns>
        /// <c>true</c> if the property value is between the valid range; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var dateTime = (DateTime)value;

            return MinValue <= dateTime && dateTime <= MaxValue;
        }

        /// <summary>
        /// Gets the errors message for invalid value.
        /// </summary>
        /// <param name="name">
        /// The property name
        /// </param>
        /// <returns>
        /// The error message
        /// </returns>
        public override string FormatErrorMessage(string name)
        {
            return string.Format("{0} must be between {1} and {2}", name, MinValue, MaxValue);
        }
    }
}