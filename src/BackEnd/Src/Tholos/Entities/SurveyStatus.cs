/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents survey status.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This enum is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="EnumMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// The enum are always thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public enum SurveyStatus
    {
        /// <summary>
        /// <para>
        /// Represents draft.
        /// </para>
        /// </summary>
        [EnumMember]
        Draft,

        /// <summary>
        /// <para>
        /// Represents published.
        /// </para>
        /// </summary> 
        [EnumMember]
        Published,

        /// <summary>
        /// <para>
        /// Represents closed.
        /// </para>
        /// </summary>
        [EnumMember]
        Closed
    }
}

