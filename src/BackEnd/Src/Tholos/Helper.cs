/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using log4net;
using Tholos.Entities;
using Tholos.Services;

namespace Tholos
{
    /// <summary>
    /// <para>
    /// Defines helper methods used in this application.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Changed CreateParticipant to use participant's username as key for search.</description>
    /// </item>
    /// <item>
    /// <description>Changed participant validation logic.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    /// <threadsafety>
    /// This class is immutable and thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    internal static class Helper
    {
        /// <summary>
        /// Represents the name of the stored procedure to retrieve prototype answer options by question id.
        /// </summary>
        internal const string GetPrototypeAnswerOptionsByQuestionIdSpName = "spGetPrototypeAnswerOptionsByQuestionId";

        /// <summary>
        /// Represents the name of the stored procedure to retrieve post testing answer options by question id.
        /// </summary>
        internal const string GetPostTestingAnswerOptionsByQuestionIdSpName =
            "spGetPostTestingAnswerOptionsByQuestionId";

        /// <summary>
        /// Represents the name of the stored procedure to get survey PrototypeCodes by survery id.
        /// </summary>
        internal const string GetPrototypeCodesBySurveyIdSpName = "spGetPrototypeCodesBySurveyId";

        /// <summary>
        /// Mask string to hide sensitive information.
        /// </summary>
        internal const string Mask = "********";

        /// <summary>
        /// Validates that a string is valid numeric and value is positive.
        /// </summary>
        /// <param name="value">The string value to validate.</param>
        /// <param name="name">The name of the parameter.</param>
        /// <returns>The number;</returns>
        /// <exception cref="ArgumentException">If <paramref name="value"/> is not valid positive number.</exception>
        internal static long CheckAndGetValidNumberAndPositive(string value, string name)
        {
            CheckNotNull(value, name);
            long longValue = 0;
            try
            {
                longValue = Convert.ToInt64(value);

            }
            catch (Exception)
            {
                throw new ArgumentException(name, string.Format("{0} must be positive number.", name));
            }
            CheckPositive(longValue, name);
            return longValue;
        }

        /// <summary>
        /// Validates that a double value is positive.
        /// </summary>
        /// <param name="value">The double value to validate.</param>
        /// <param name="name">The name of the parameter.</param>
        /// <exception cref="ArgumentException">If <paramref name="value"/> is not positive.</exception>
        internal static void CheckPositive(double value, string name)
        {
            if (value < 1)
            {
                throw new ArgumentException(name, string.Format("{0} must be positive.", name));
            }
        }

        /// <summary>
        /// Validates that a double value is positive.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <param name="value">The double value to validate.</param>
        /// <param name="name">The name of the parameter.</param>
        /// <exception cref="ArgumentException">If <paramref name="value"/> is not positive.</exception>
        internal static void CheckLegnth(int length, string value, string name)
        {
            if (value.Length > length)
            {
                throw new ArgumentException(name, string.Format("{0} length must be less than {1}.", name, length));
            }
        }

        /// <summary>
        /// Validates that a numeric value is positive.
        /// </summary>
        /// <param name="value">The numeric value to validate.</param>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="b"></param>
        /// <param name="acceptZero">The flag if true zero will be accepted as valid</param>
        /// <exception cref="ArgumentException">If <paramref name="value"/> is not positive.</exception>
        internal static void CheckPositive(long value, string name, bool acceptZero = false)
        {
            if (acceptZero)
            {
                if (value < 0)
                {
                    throw new ArgumentException(name, string.Format("{0} must be positive.", name));
                }
            }
            else
            {
                if (value < 1)
                {
                    throw new ArgumentException(name, string.Format("{0} must be positive.", name));
                }
            }
        }

        /// <summary>
        /// <para>
        /// Checks whether the given object is null.
        /// </para>
        /// </summary>
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <param name="parameterName">
        /// The actual parameter name of the argument being checked.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If value is null.
        /// </exception>
        internal static void CheckNotNull(object value, string parameterName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(
                    parameterName, string.Format("The parameter '{0}' can not be null.", parameterName));
            }
        }

        /// <summary>
        /// <para>
        /// Checks whether the given object is null.
        /// </para>
        /// </summary>
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <param name="parameterName">
        /// The actual parameter name of the property being checked.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If value is null.
        /// </exception>
        internal static void CheckPropertyNotNull(object value, string parameterName)
        {
            if (value == null)
            {
                throw new ArgumentException(
                    parameterName, string.Format("The property '{0}' can not be null.", parameterName));
            }
        }

        /// <summary>
        /// <para>
        /// Checks whether the given string is null or empty.
        /// </para>
        /// </summary>
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <param name="parameterName">
        /// The actual parameter name of the argument being checked.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If the string is null.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// If the given string is empty string.
        /// </exception>
        internal static void CheckNotNullOrEmpty(string value, string parameterName)
        {
            CheckNotNull(value, parameterName);

            if (value.Trim().Length == 0)
            {
                throw new ArgumentException(
                    string.Format("The parameter '{0}' can not be empty string.", parameterName), parameterName);
            }
        }

        /// <summary>
        /// <para>
        /// Checks whether the given string is null or empty.
        /// </para>
        /// </summary>
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <param name="parameterName">
        /// The actual parameter name of the property being checked.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If the string is null.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// If the given string is empty string.
        /// </exception>
        internal static void CheckPropertyNotNullOrEmpty(string value, string parameterName)
        {
            CheckPropertyNotNull(value, parameterName);

            if (value.Trim().Length == 0)
            {
                throw new ArgumentException(
                    string.Format("The property '{0}' can not be empty string.", parameterName), parameterName);
            }
        }

        /// <summary>
        /// <para>
        /// Checks whether the given value is null or empty, or positive integer.
        /// </para>
        /// </summary>
        /// <param name="value">
        /// The value to check.
        /// </param>
        /// <param name="name">
        /// The actual property name.
        /// </param>
        /// <exception cref="ConfigurationException">
        /// If value is null or empty string, or not positive integer.
        /// </exception>
        internal static void CheckConfiguration(object value, string name)
        {
            if ((value is int) && (int)value <= 0)
            {
                throw new ConfigurationException(string.Format("{0} should be positive number.", name));
            }
            if (value == null || value is string && ((string)value).Trim().Length == 0)
            {
                throw new ConfigurationException(string.Format(
                   "Instance property '{0}' wasn't configured properly.", name));
            }
        }

        /// <summary>
        /// Checks whether the given search criteria is <c>null</c> or incorrect.
        /// </summary>
        ///
        /// <param name="filter">The filter to check.</param>
        /// <param name="allowedPropertyNames">The allowed values for the <c>SortColumn</c> property.</param>
        ///
        /// <exception cref="ArgumentNullException">If the <paramref name="filter"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">If the <paramref name="filter"/> is incorrect.</exception>
        internal static void CheckSearchCriteria(SearchFilter filter, params string[] allowedPropertyNames)
        {
            CheckNotNull(filter, "filter");
            if (filter.PageNumber < 0)
            {
                throw new ArgumentException("Page number can't be negative.", "filter");
            }

            if (filter.PageNumber > 0 && filter.PageSize < 1)
            {
                throw new ArgumentException("Page size should be positive, if page number is positive.", "filter");
            }

            if (!string.IsNullOrWhiteSpace(filter.SortColumn) && !allowedPropertyNames.Contains(filter.SortColumn))
            {
                throw new ArgumentException("Sort column should be valid entity property name.", "filter");
            }
        }

        /// <summary>
        /// This is a helper method to provide logging wrapper.
        /// </summary>
        ///
        /// <remarks>
        /// Any exception will throw to caller directly.
        /// </remarks>
        ///
        /// <param name="call">
        /// The delegation to execute the business logic.
        /// </param>
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="parameters">
        /// The parameters in the delegation method.
        /// </param>
        internal static void LoggingWrapper(ILog logger, Action call, params object[] parameters)
        {
            // Log method entry
            var callingMethod = new StackTrace().GetFrame(1).GetMethod();
            LogMethodEntry(callingMethod, logger, parameters);

            Process(call, logger, callingMethod);

            // Log method exit
            LogMethodExit(callingMethod, logger);
        }

        /// <summary>
        /// This is a helper method to provide logging wrapper.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// The return type.
        /// </typeparam>
        ///
        /// <remarks>
        /// Any exception will throw to caller directly.
        /// </remarks>
        ///
        /// <param name="call">
        /// The delegation to execute the business logic.
        /// </param>
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="parameters">
        /// The parameters in the delegation method.
        /// </param>
        ///
        /// <returns>
        /// The value returned by <paramref name="call"/>.
        /// </returns>
        internal static T LoggingWrapper<T>(ILog logger, Func<T> call, params object[] parameters)
        {
            // Log method entry
            var callingMethod = new StackTrace().GetFrame(1).GetMethod();
            LogMethodEntry(callingMethod, logger, parameters);

            T ret = default(T);

            Process(() =>
            {
                ret = call();
            }, logger, callingMethod);

            // Log method exit with return value
            return LogReturnValue(callingMethod, logger, ret);
        }

        /// <summary>
        /// Processes the specified action and wraps it with common error handling logic.
        /// </summary>
        /// <param name="call">
        /// The delegation to execute the business logic.
        /// </param>
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="callingMethod">The method where the logging occurs.</param>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from the source exception.
        /// </exception>
        private static void Process(Action call, ILog logger, MethodBase callingMethod)
        {
            try
            {
                // Delegation
                call();
            }
            catch (ArgumentException ex)
            {
                throw LogExceptionAndWrap(ex, logger, callingMethod);
            }
            catch (ExportingException ex)
            {
                throw LogExceptionAndWrap(ex, logger, callingMethod);
            }
            catch (SecurityTokenException ex)
            {
                throw LogExceptionAndWrap(new TholosException(ex.Message, ex.InnerException), logger, callingMethod);
            }
            catch (TholosException ex)
            {
                throw LogExceptionAndWrap(ex, logger, callingMethod);
            }
            catch (WebFaultException<ServiceFaultDetail>)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw LogExceptionAndWrap(new TholosException(ex.Message, ex.InnerException), logger, callingMethod);
            }
        }

        /// <summary>
        /// <para>
        /// Gets the string representation of the given object.
        /// </para>
        /// </summary>
        /// <param name="obj">
        /// The object to describe.
        /// </param>
        /// <returns>
        /// The string representation of the object.
        /// </returns>
        private static string GetObjectDescription(object obj)
        {
            if (obj == null)
            {
                return "NULL";
            }
            if (obj is string || obj.GetType().IsValueType)
            {
                return obj.ToString();
            }

            try
            {
                var result = new JavaScriptSerializer().Serialize(obj);
                // Avoid exposing the token
                result = Regex.Replace(result, "\"Password\":\"[^\"]*\"", "\"Password\":\"********\"");
                return result;
            }
            catch
            {
                return "Object cannot be represented.";
            }
        }

        /// <summary>
        /// Logs a method entry at DEBUG level.
        /// </summary>
        ///
        /// <param name="method">
        /// The method where the logging occurs.
        /// </param>
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="parameters">
        /// The method parameters.
        /// </param>
        ///
        /// <returns>
        /// The method start date time.
        /// </returns>
        /// <remarks>The internal exception may be thrown directly.</remarks>
        private static void LogMethodEntry(MethodBase method, ILog logger, params object[] parameters)
        {
            // Create a string format to display parameters
            var logFormat = new StringBuilder();
            var pis = method.GetParameters();
            var methodName = string.Format("{0}.{1}", method.DeclaringType.Name, method.Name);
            if (pis.Length != parameters.Length)
            {
                throw new ArgumentException(string.Format(
                    "The number of provided parameters for method '{0}' is wrong.", methodName), "parameters");
            }
            // log method entry
            logFormat.AppendFormat("[Entering method {0}]", methodName).AppendLine();

            // log input parameters
            if (parameters.Any())
            {
                logFormat.AppendLine("[Input parameters[");
                for (var i = 0; i < pis.Length; i++)
                {
                    logFormat.Append("\t").Append(pis[i].Name).Append(": ");
                    logFormat.AppendLine(GetObjectDescription(parameters[i]));
                }
                logFormat.Append("]");
            }
            // Log
            LogDebug(logger, logFormat.ToString());
        }

        /// <summary>
        /// Logs a method exit at DEBUG level.
        /// </summary>
        ///
        /// <param name="method">
        /// The method where the logging occurs.
        /// </param>
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="start">
        /// The method start date time.
        /// </param>
        private static void LogMethodExit(MethodBase method, ILog logger)
        {
            LogDebug(logger, string.Format("[Exiting method {0}.{1}]",
                method.DeclaringType.Name, method.Name));
        }

        /// <summary>
        /// Logs the return value and method exit on DEBUG level.
        /// </summary>
        ///
        /// <typeparam name="R">
        /// The method return type.
        /// </typeparam>
        ///
        /// <param name="method">
        /// The method where the logging occurs.
        /// </param>
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="returnValue">
        /// The return value to log.
        /// </param>
        /// <param name="start">
        /// The method start date time.
        /// </param>
        private static R LogReturnValue<R>(MethodBase method, ILog logger, R returnValue)
        {
            LogDebug(logger,
                string.Format("[Exiting method {0}.{1}] [Output parameter : {2}]",
                method.DeclaringType.Name, method.Name, GetObjectDescription(returnValue)));
            return returnValue;
        }


        ///  <summary>
        ///  Logs the given exception with ERROR level and
        ///  return it wrapped into <see cref="WebFaultException{ServiceFaultDetail}"/>.
        ///  </summary>
        /// 
        ///  <remarks>
        ///  If <paramref name="method"/> is <c>null</c>, the calling
        ///  method will be used as a source of exception.
        ///  </remarks>
        /// 
        ///  <param name="exception">The exception to log.</param>
        /// <param name="logger">The logger instance to be used for logging.</param>
        /// <param name="method">The method where exception was raised.</param>
        /// 
        ///  <returns>The exception wrapped into <see cref="WebFaultException"/>.</returns>
        private static WebFaultException<ServiceFaultDetail> LogExceptionAndWrap(Exception exception, ILog logger
            , MethodBase method = null)
        {
            if (method == null)
            {
                // get info for previous method in stack
                method = new StackFrame(1).GetMethod();
            }

            // log exception
            LogError(logger, string.Format("[Error in method {0}.{1}:\nDetails:\n{2}]",
                method.DeclaringType.Name, method.Name, exception));

            // define status code
            HttpStatusCode statusCode;
            if (exception is ArgumentException)
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            else if (exception is EntityNotFoundException)
            {
                statusCode = HttpStatusCode.NotFound;
            }
            else
            {
                statusCode = HttpStatusCode.InternalServerError;
            }

            // return fault exception, so that caller WCF service method can throw it
            return new WebFaultException<ServiceFaultDetail>(new ServiceFaultDetail
            {
                ErrorType = exception.GetType().Name,
                ErrorMessage = exception.Message
            }, statusCode);
        }

        /// <summary>
        /// Logs the given message on DEBUG level.
        /// </summary>
        ///
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="message">
        /// The message to log.
        /// </param>
        private static void LogDebug(ILog logger, string message)
        {
            if (logger == null)
            {
                return;
            }
            try
            {
                logger.Debug(message);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Logs the given message on ERROR level.
        /// </summary>
        ///
        /// <param name="logger">
        /// The logger instance to be used for logging.
        /// </param>
        /// <param name="message">
        /// The message to log.
        /// </param>
        private static void LogError(ILog logger, string message)
        {
            if (logger == null)
            {
                return;
            }
            try
            {
                logger.Error(message);
            }
            catch
            {
            }
        }

        ///<summary>
        /// Gets the string value from the reader.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The string value.
        /// </returns>
        internal static string GetStringValue(IDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? null : reader[idx].ToString();
        }


        /// <summary>
        /// Gets the DateTime value from the reader.
        /// </summary>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The DateTime value.
        /// </returns>
        internal static DateTime GetDateTimeValue(IDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? default(DateTime) : Convert.ToDateTime(reader[idx]);
        }

        /// <summary>
        /// Gets the nullabe DateTime value from the reader.
        /// </summary>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The nullable DateTime value.
        /// </returns>
        internal static DateTime? GetNullableDateTimeValue(IDataReader reader, int idx)
        {
            DateTime? value = null;
            if (!reader.IsDBNull(idx))
            {
                value = Convert.ToDateTime(reader[idx]);
            }
            return value;
        }

        /// <summary>
        /// Gets the Boolean value from the reader.
        /// </summary>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The Boolean value.
        /// </returns>
        internal static bool GetBoolValue(IDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? default(bool) : Convert.ToBoolean(reader[idx]);
        }

        /// <summary>
        /// Gets the Int32 value from the reader.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The Int32 value.
        /// </returns>
        internal static int GetIntValue(IDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? default(int) : Convert.ToInt32(reader[idx]);
        }

        /// <summary>
        /// Gets the Int64 value from the reader.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The Int64 value.
        /// </returns>
        internal static long GetLongValue(IDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? default(long) : Convert.ToInt64(reader[idx]);
        }

        /// <summary>
        /// Gets the double value from the reader.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The double value.
        /// </returns>
        internal static double GetDoubleValue(IDataReader reader, int idx)
        {
            return reader.IsDBNull(idx) ? default(double) : Convert.ToDouble(reader[idx]);
        }

        /// <summary>
        /// Gets the nullable double value from the reader.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="idx">
        /// The data index.
        /// </param>
        ///
        /// <returns>
        /// The nullable double value.
        /// </returns>
        internal static double? GetNullableDoubleValue(IDataReader reader, int idx)
        {
            double? value = null;
            if (!reader.IsDBNull(idx))
            {
                value = Convert.ToDouble(reader[idx]);
            }
            return value;
        }

        /// <summary>
        /// Calls the given delegation with given data connection.
        /// </summary>
        ///
        /// <remarks>
        /// If the exception from <paramref name="call"/> is not extended from PersistenceException,
        /// it will be wrapped by PersistenceException.
        /// </remarks>
        ///
        /// <param name="conn">
        /// The database connection.
        /// </param>
        ///
        /// <param name="call">
        /// The delegation to call with the database connection.
        /// </param>
        ///
        /// <exception cref="ArgumentException">
        /// If any arguments are invalid.
        /// </exception>
        /// <exception cref="EntityNotFoundException">
        /// If entity is not found for update operation.
        /// </exception>
        /// <exception cref="PersistenceException">
        /// If the data access error occurred in this method.
        /// </exception>
        internal static void DbCall(IDbConnection conn, Action<IDbConnection> call)
        {
            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                call(conn);
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (EntityNotFoundException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw (e is PersistenceException) ? e :
                    new PersistenceException("Error occurred during accessing the database", e);
            }
        }

        /// <summary>
        /// Helper method executes the update or delete SP.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="spName">
        /// The SP name.
        /// </param>
        /// <param name="filterCreteria">
        /// The filter crieteria used for update or delete.
        /// </param>
        /// <param name="parameters">
        /// The SP parameters.
        /// </param>
        /// <param name="isUpdate">
        /// The flag to show is update method.
        /// </param>
        internal static void ExecuteUpdateOrDeleteSp<T>(IDbConnection connection, string spName,
            string filterCreteria, IEnumerable<KeyValuePair<string, object>> parameters,
            bool isUpdate = true) where T : class
        {
            DbCall(connection, conn =>
            {
                using (var command = CreateCommand(conn, spName, parameters))
                {
                    int rows = command.ExecuteNonQuery();
                    if (rows == 0)
                    {
                        throw new EntityNotFoundException(
                            BuildNotFoundErrorMessage<T>(filterCreteria, isUpdate ? "Updating" : "Deleting"));
                    }
                }
            });
        }

        ///  <summary>
        ///  Helper method executes the SP.
        ///  </summary>
        /// 
        ///  <remarks>
        ///  The internal exception may be thrown directly.
        ///  </remarks>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        /// <param name="spName">
        ///     The SP name.
        /// </param>
        /// <param name="parameters">
        ///     The SP parameters.
        /// </param>
        internal static void ExecuteSp(IDbConnection connection, string spName,
            IEnumerable<KeyValuePair<string, object>> parameters)
        {
            DbCall(connection, conn =>
            {
                using (var command = CreateCommand(conn, spName, parameters))
                {
                    command.ExecuteNonQuery();
                }
            });
        }

        ///  <summary>
        ///  Helper method executes the SP and get output param.
        ///  </summary>
        /// 
        ///  <remarks>
        ///  The internal exception may be thrown directly.
        ///  </remarks>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        /// <param name="spName">
        ///     The SP name.
        /// </param>
        /// <param name="parameters">
        ///     The SP In parameters.
        /// </param>
        /// <param name="outParamterName">
        ///     The SP Out parameter.
        /// </param>
        internal static long ExecuteSpWithReturnOutParam(IDbConnection connection, string spName,
            IEnumerable<KeyValuePair<string, object>> parameters, string outParamterName)
        {
            long result = 0;
            DbCall(connection, conn =>
            {
                using (var command = CreateCommand(conn, spName, parameters))
                {
                    var outputIdParam = command.CreateParameter();
                    outputIdParam.ParameterName = "@" + outParamterName;
                    outputIdParam.DbType = DbType.Int64;
                    outputIdParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(outputIdParam);
                    command.ExecuteNonQuery();

                    result = Convert.ToInt64(outputIdParam.Value);

                }
            });
            return result;
        }

        ///  <summary>
        ///  <para>
        ///  Creates a data command to be used to execute a stored procedure.
        ///  </para>
        ///  </summary>
        /// 
        ///  <remarks>
        ///  <para>All thrown exceptions will be propagated to caller method.</para>
        ///  </remarks>
        /// <param name="connection">
        ///     The database connection to be used.
        /// </param>
        /// <param name="spName">
        ///     The stored procedure name.
        /// </param>
        /// <param name="parameters">
        ///     The command parameters.
        /// </param>
        /// <returns>
        ///  The data command instance.
        ///  </returns>
        internal static IDbCommand CreateCommand(IDbConnection connection, string spName,
            IEnumerable<KeyValuePair<string, object>> parameters)
        {
            var command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = spName;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    var dbParameter = command.CreateParameter();
                    dbParameter.ParameterName = "@" + parameter.Key;
                    dbParameter.Value = parameter.Value ?? DBNull.Value;
                    command.Parameters.Add(dbParameter);
                }
            }
            return command;
        }

        ///  <summary>
        ///  Helper method executes the retrieve SP.
        ///  </summary>
        /// 
        ///  <remarks>
        ///  The internal exception may be thrown directly.
        ///  </remarks>
        ///  <typeparam name="T">The entity type</typeparam>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        /// <param name="spName">
        ///     The SP name.
        /// </param>
        /// <param name="program">
        ///     The custom code to read the data.
        /// </param>
        /// <param name="parameters">
        ///     The SP parameters.
        /// </param>
        /// <returns>The entity</returns>
        internal static T ExecuteRetrieveSp<T>(IDbConnection connection, string spName, Func<IDataReader, T> program,
            IEnumerable<KeyValuePair<string, object>> parameters = null)
            where T : class
        {
            T result = null;
            DbCall(connection, conn =>
            {
                using (var command = CreateCommand(conn, spName, parameters))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        result = program(reader);
                    }
                }
            });
            return result;
        }

        /// <summary>
        /// Checks whether the entity with given id exists.
        /// </summary>
        /// <param name="table">The table name</param>
        /// <param name="id">The entity id</param>
        /// <param name="conn">The IDbConnection</param>
        /// <exception cref="EntityNotFoundException">If no such entity is found</exception>
        internal static void CheckExistence(string table, long id, IDbConnection conn)
        {
            try
            {
                using (var command = conn.CreateCommand())
                {
                    command.CommandText = "SELECT COUNT(ID) FROM " + table + " WHERE Id=" + id;
                    command.CommandType = CommandType.Text;
                    int count = Convert.ToInt16(command.ExecuteScalar());
                    if (count == 0)
                    {
                        throw new EntityNotFoundException("Entity with id " + id + " does not exist.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex is EntityNotFoundException
                    ? ex
                    : new PersistenceException("There is error when check the existence of entity with id " + id,
                        ex);
            }

        }

        /// <summary>
        /// Build entity not found exception message
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="filterCreteria">The filter crieteria used for update or delete.</param>
        /// <param name="methdName">the method name</param>
        /// <returns>The entity not found exception message</returns>
        internal static string BuildNotFoundErrorMessage<T>(string filterCreteria, string methdName)
            where T : class
        {
            return string.Format("The {0} entity with {1} is not found during {2}.",
                typeof(T).Name, filterCreteria, methdName);
        }

        /// <summary>
        /// Represent table column names participant credentials for role model.
        /// </summary>
        internal static readonly string[] ParticipantCredentialsColumnNames =
        {
            "Id", "SurveyId", "Username", "Password","Used"
        };

        /// <summary>
        /// Represent table column names User for role model.
        /// </summary>
        internal static readonly string[] UserColumnNames =
        {
            "Id", "Name", "UserpicPath"
        };

        /// <summary>
        ///  Represent table column names for role model.
        /// </summary>
        internal static readonly string[] RoleColumnNames = { "Id", "Name" };

        /// <summary>
        ///  Represent table column names for Participant model.
        /// </summary>
        internal static readonly string[] ParticipantColumnNames =
        {
           "Id", "Username", "SurveyId","BirthDate", "Weight", "HeightFeet", "HeightInches"
        };

        /// <summary>
        ///  Represent table column names for Answer model.
        /// </summary>
        internal static readonly string[] AnswerColumnNames =
        {
           "Id", "QuestionId", "AnswerType", "Value", "AnswerOptionId","Input"
        };

        /// <summary>
        ///  Represent table column names for Question model.
        /// </summary>
        internal static readonly string[] QuestionColumnNames =
        {
            "Id", "Text", "Conditional", "QuestionType", "ConditionId",
            "ConditionalOnQuestionId", "ConditionalOnAnswerId"
        };

        /// <summary>
        ///  Represent table column names for Question model with Question number.
        /// </summary>
        internal static readonly string[] SurveyQuestionColumnNames =
        {
            "Id", "Text", "Conditional", "QuestionType", "QuestionNumber", "ConditionId",
            "ConditionalOnQuestionId", "ConditionalOnAnswerId"
        };

        /// <summary>
        ///  Represent table column names for AnswerOptions model.
        /// </summary>
        internal static readonly string[] AnswerOptionsColumnNames =
        {
            "Id", "QuestionId", "AnswerType", "Value", "Label",
            "FromValueLabel", "ToValueLabel", "FromValue", "ToValue", "Increment"
        };

        /// <summary>
        ///  Represent table column names for Survey model.
        /// </summary>
        internal static readonly string[] SurveyColumnNames =
        {
            "Id", "Name", "CellName", "ParticipantsNumber", "Status", "DateCreated", "CompletedSurveysNumber",
            "Draft"
        };

        /// <summary>
        ///  Represent table column names for ParticipantSurvey model.
        /// </summary>
        internal static readonly string[] ParticipantSurveyColumnNames =
        {
            "Id", "ParticipantId", "SurveyId", "CurrentPrototypeTestId"
        };

        /// <summary>
        ///  Represent table column names for PrototypeTest model.
        /// </summary>
        internal static readonly string[] PrototypeTestColumnNames =
        {
            "Id", "PrototypeCode", "Iteration", "BeforeWeight", "AfterWeight",
            "BeforePhotoUri", "AfterPhotoUri", "BeforePhotoLocalPath", "AfterPhotoLocalPath","Completed"
        };

        /// <summary>
        /// Gets the index of columns in the given reader.
        /// </summary>
        ///
        /// <param name="reader">
        /// The data reader.
        /// </param>
        /// <param name="names">
        /// The column names.
        /// </param>
        ///
        /// <returns>
        /// The according index array.
        /// </returns>
        internal static int[] GetReaderIndex(IDataReader reader, params string[] names)
        {
            var ret = new int[names.Length];
            for (var i = 0; i < names.Length; ++i)
            {
                ret[i] = reader.GetOrdinal(names[i]);
            }
            return ret;
        }

        /// <summary>
        /// Helper method read entity model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="columns">
        /// The column names.
        /// </param>
        /// <param name="readEntity">
        /// The delegate to read entity with reader and column index.
        /// </param>
        ///<returns>The entity</returns>
        internal static T ReaderEntity<T>(IDataReader reader, string[] columns, Func<IDataReader, int[], T> readEntity)
        {
            var result = default(T);
            var idx = GetReaderIndex(reader, columns);
            if (reader.Read())
            {
                result = readEntity(reader, idx);
            }
            return result;
        }


        /// <summary>
        /// Helper method read model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="columns">
        /// The column names.
        /// </param>
        /// <param name="readEntity">
        /// The delegate to read entity with reader and column index.
        /// </param>
        ///<returns>The entity list.</returns>
        internal static IList<T> ReaderEntityList<T>(IDataReader reader, string[] columns,
            Func<IDataReader, int[], T> readEntity)
        {
            var result = new List<T>();
            var idx = GetReaderIndex(reader, columns);
            while (reader.Read())
            {
                result.Add(readEntity(reader, idx));
            }
            return result;
        }

        /// <summary>
        /// Helper method read string list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="index">
        /// The column index.
        /// </param>
        ///<returns>The string list</returns>
        internal static IList<string> ReadStringList(IDataReader reader, int index = 0)
        {
            var result = new List<string>();
            while (reader.Read())
            {
                result.Add(GetStringValue(reader, index));
            }
            return result;
        }

        /// <summary>
        /// Helper method read participant credentials model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The participant credentials.</returns>
        internal static ParticipantCredentials ReaderParticipantCredentials(IDataReader reader, int[] idx)
        {
            int index = 0;
            return new ParticipantCredentials
            {
                Id = GetLongValue(reader, idx[index++]),
                SurveyId = GetLongValue(reader, idx[index++]),
                Username = GetStringValue(reader, idx[index++]),
                Password = GetStringValue(reader, idx[index++]),
                Used = GetBoolValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read participant credentials model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The participant credentials.</returns>
        internal static ParticipantCredentials ReaderParticipantCredentials(IDataReader reader)
        {
            return ReaderEntity(reader, ParticipantCredentialsColumnNames, ReaderParticipantCredentials);
        }

        /// <summary>
        /// Helper method read participant credentials model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly list.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The participant credentials list.</returns>
        internal static IList<ParticipantCredentials> ReaderParticipantCredentialsList(IDataReader reader)
        {
            return ReaderEntityList(reader, ParticipantCredentialsColumnNames, ReaderParticipantCredentials);
        }

        /// <summary>
        /// Helper method read user model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The user.</returns>
        internal static User ReaderUser(IDataReader reader, int[] idx)
        {
            int index = 0;
            return new User
            {
                Id = GetLongValue(reader, idx[index++]),
                Name = GetStringValue(reader, idx[index++]),
                UserpicPath = GetStringValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read user model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The user.</returns>
        internal static User ReaderUser(IDataReader reader)
        {
            return ReaderEntity(reader, UserColumnNames, ReaderUser);
        }

        /// <summary>
        /// Helper method read role list model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The role list.</returns>
        internal static IList<Role> ReaderRoles(IDataReader reader)
        {
            return ReaderEntityList(reader, RoleColumnNames, delegate(IDataReader dataReader, int[] idx)
            {
                int index = 0;
                return new Role
                {
                    Id = GetLongValue(dataReader, idx[index++]),
                    Name = GetStringValue(dataReader, idx[index])
                };
            });
        }

        /// <summary>
        /// Helper method read Participant model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The Participant.</returns>
        internal static Participant ReaderParticipant(IDataReader reader, int[] idx)
        {
            int index = 0;
            return new Participant
            {
                Id = GetLongValue(reader, idx[index++]),
                Username = GetStringValue(reader, idx[index++]),
                SurveyId = GetLongValue(reader, idx[index++]),
                BirthDate = GetNullableDateTimeValue(reader, idx[index++]),
                Weight = GetIntValue(reader, idx[index++]),
                HeightFeet = GetIntValue(reader, idx[index++]),
                HeightInches = GetIntValue(reader, idx[index]),
            };
        }

        /// <summary>
        /// Helper method read Participant model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The Participant.</returns>
        internal static Participant ReaderParticipant(IDataReader reader)
        {
            return ReaderEntity(reader, ParticipantColumnNames, ReaderParticipant);
        }

        /// <summary>
        /// Helper method read Answer model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The Answer list.</returns>
        internal static Answer ReaderAnswer(IDataReader reader, int[] idx)
        {
            int index = 0;
            Answer entity = null;
            var id = GetLongValue(reader, idx[index++]);
            var questionId = GetLongValue(reader, idx[index++]);
            var answerType = (AnswerType)Enum.Parse(typeof(AnswerType), GetStringValue(reader, idx[index++]), true);
            if (AnswerType.RangeAnswer.Equals(answerType))
            {
                RangeAnswer rangeAnswer = new RangeAnswer();
                rangeAnswer.Value = GetIntValue(reader, idx[index]);
                entity = rangeAnswer;

            }
            else if (AnswerType.SingleAnswer.Equals(answerType))
            {
                var singleAnswer = new SingleAnswer();
                index++;//skip value column
                singleAnswer.AnswerOptionId = GetLongValue(reader, idx[index]);
                entity = singleAnswer;
            }
            else if (AnswerType.SingleAnswerInput.Equals(answerType))
            {
                var singleInputAnswer = new SingleInputAnswer();
                index++; //skip value column
                singleInputAnswer.AnswerOptionId = GetLongValue(reader, idx[index++]);
                singleInputAnswer.Input = GetStringValue(reader, idx[index]);
                entity = singleInputAnswer;
            }
            entity.Id = id;
            entity.QuestionId = questionId;
            entity.AnswerType = answerType;
            return entity;
        }

        /// <summary>
        /// Helper method read Answer model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The Answer list.</returns>
        internal static IList<Answer> ReaderAnswer(IDataReader reader)
        {
            return ReaderEntityList(reader, AnswerColumnNames, ReaderAnswer);
        }

        ///  <summary>
        ///  Helper method read Question model.
        ///  </summary>
        /// 
        ///  <remarks>
        ///  The internal exception may be thrown directly.
        ///  </remarks>
        /// 
        ///  <param name="reader">
        ///  The database reader.
        ///  </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        /// <returns>The Question.</returns>
        internal static Question ReaderQuestion(IDataReader reader, int[] idx)
        {
            int index = 0;
            var question = new Question
            {
                Id = GetLongValue(reader, idx[index++]),
                Text = GetStringValue(reader, idx[index++]),
                Conditional = GetBoolValue(reader, idx[index++]),
                QuestionType = (QuestionType)Enum.Parse(typeof(QuestionType),
                         GetStringValue(reader, idx[index++]), true),

            };
            if (question.Conditional)
            {
                question.Condition = new QuestionCondition
                {
                    Id = GetLongValue(reader, idx[index++]),
                    ConditionalOnQuestionId = GetLongValue(reader, idx[index++]),
                    ConditionalOnAnswerId = GetLongValue(reader, idx[index])
                };
            }
            return question;
        }

        /// <summary>
        /// Helper method read Question model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The Question list.</returns>
        internal static IList<Question> ReaderQuestionsList(IDataReader reader)
        {
            return LabelAndSort(ReaderEntityList(reader, QuestionColumnNames, ReaderQuestion));
        }

        /// <summary>
        /// Helper method read AnswerOption model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The AnswerOption list.</returns>
        internal static AnswerOption ReaderAnswerOptions(IDataReader reader, int[] idx)
        {
            int index = 0;
            AnswerOption entity = null;
            var id = GetLongValue(reader, idx[index++]);
            var questionId = GetLongValue(reader, idx[index++]);
            var answerType = (AnswerType)Enum.Parse(typeof(AnswerType), GetStringValue(reader, idx[index++]), true);
            if (AnswerType.SingleAnswer.Equals(answerType))
            {
                var singleAnswerOption = new SingleAnswerOption();
                singleAnswerOption.Value = GetIntValue(reader, idx[index++]);
                singleAnswerOption.Label = GetStringValue(reader, idx[index]);
                entity = singleAnswerOption;

            }
            else if (AnswerType.SingleAnswerInput.Equals(answerType))
            {
                var singleAnswerInputOption = new SingleAnswerInputOption();
                singleAnswerInputOption.Value = GetIntValue(reader, idx[index++]);
                singleAnswerInputOption.Label = GetStringValue(reader, idx[index]);
                entity = singleAnswerInputOption;
            }
            else if (AnswerType.RangeAnswer.Equals(answerType))
            {
                var rangeAnswerOption = new RangeAnswerOption();
                index++; //skip value column
                index++; //skip label column
                rangeAnswerOption.FromValueLabel = GetStringValue(reader, idx[index++]);
                rangeAnswerOption.ToValueLabel = GetStringValue(reader, idx[index++]);
                rangeAnswerOption.FromValue = GetIntValue(reader, idx[index++]);
                rangeAnswerOption.ToValue = GetIntValue(reader, idx[index++]);
                rangeAnswerOption.Increment = GetIntValue(reader, idx[index]);
                entity = rangeAnswerOption;
            }
            else
            {
                entity = new AnswerOption();
            }
            entity.Id = id;
            entity.QuestionId = questionId;
            entity.Type = answerType;
            return entity;
        }

        /// <summary>
        /// Helper method read AnswerOption model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The AnswerOption list.</returns>
        internal static IList<AnswerOption> ReaderAnswerOptions(IDataReader reader)
        {
            return ReaderEntityList(reader, AnswerOptionsColumnNames, ReaderAnswerOptions);
        }

        /// <summary>
        /// Helper method read Survey model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The Survey.</returns>
        internal static Survey ReaderSurvey(IDataReader reader, int[] idx)
        {
            int index = 0;
            return new Survey
            {
                Id = GetLongValue(reader, idx[index++]),
                Name = GetStringValue(reader, idx[index++]),
                CellName = GetStringValue(reader, idx[index++]),
                ParticipantsNumber = GetIntValue(reader, idx[index++]),
                Status = (SurveyStatus)Enum.Parse(typeof(SurveyStatus), GetStringValue(reader, idx[index++]), true),
                DateCreated = GetDateTimeValue(reader, idx[index++]),
                CompletedSurveysNumber = GetIntValue(reader, idx[index++]),
                Draft = GetBoolValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read Survey model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The Survey.</returns>
        internal static Survey ReaderSurvey(IDataReader reader)
        {
            Survey ret= ReaderEntity(reader, SurveyColumnNames, ReaderSurvey);
            return ret;
        }

        /// <summary>
        /// Helper method read Survey model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The Survey list.</returns>
        internal static IList<Survey> ReaderSurveyList(IDataReader reader)
        {
            return ReaderEntityList(reader, SurveyColumnNames, ReaderSurvey);
        }

        /// <summary>
        /// Helper method read PrototypeCode model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The PrototypeCode.</returns>
        private static PrototypeCode ReaderPrototypeCode(IDataReader reader, int[] idx)
        {
            int index = 0;
            return new PrototypeCode
            {
                Id = GetLongValue(reader, idx[index++]),
                Code = GetStringValue(reader, idx[index++]),
                PrototypesPerCode = GetIntValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read PrototypeCode model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The PrototypeCode list.</returns>
        internal static IList<PrototypeCode> ReaderPrototypeCode(IDataReader reader)
        {
            string[] prototypeCodeColumnNames = { "Id", "Code", "PrototypesPerCode" };
            return ReaderEntityList(reader, prototypeCodeColumnNames, ReaderPrototypeCode);
        }

        /// <summary>
        /// Helper method read Survey PrototypeQuestions and PostTestQuestions dictionary.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The Survey PrototypeQuestions and PostTestQuestions dictionary.</returns>
        internal static IList<IndexedQuestion> ReaderSurveyQuestionsList(IDataReader reader)
        {
            var result = new List<IndexedQuestion>();
            var idx = GetReaderIndex(reader, SurveyQuestionColumnNames);
            while (reader.Read())
            {
                int index = 0;
                var question = new Question
                {
                    Id = GetLongValue(reader, idx[index++]),
                    Text = GetStringValue(reader, idx[index++]),
                    Conditional = GetBoolValue(reader, idx[index++]),
                    QuestionType = (QuestionType)Enum.Parse(typeof(QuestionType),
                             GetStringValue(reader, idx[index++]), true),

                };
                var position = GetIntValue(reader, idx[index++]);
                if (question.Conditional)
                {
                    question.Condition = new QuestionCondition
                    {
                        Id = GetLongValue(reader, idx[index++]),
                        ConditionalOnQuestionId = GetLongValue(reader, idx[index++]),
                        ConditionalOnAnswerId = GetLongValue(reader, idx[index])
                    };
                }
                result.Add(new IndexedQuestion
                {
                    Position = position,
                    Question = question
                });
            }
            return result;
        }

        /// <summary>
        /// Helper method read quick survey access.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The quick survey access.</returns>
        internal static QuickAccessSurvey ReaderQuickAccessSurvey(IDataReader reader, int[] idx)
        {
            var index = 0;
            return new QuickAccessSurvey
            {
                SurveyId = GetLongValue(reader, idx[index++]),
                SurveyName = GetStringValue(reader, idx[index++]),
                CellName = GetStringValue(reader, idx[index++]),
                Position = GetIntValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read quick survey access.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The quick survey access.</returns>
        internal static IList<QuickAccessSurvey> ReaderQuickAccessSurveyList(IDataReader reader)
        {
            string[] columns = { "SurveyId", "Name", "CellName", "Position" };

            return ReaderEntityList(reader, columns, ReaderQuickAccessSurvey);
        }

        /// <summary>
        /// Helper method read ParticipantSurvey model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The ParticipantSurvey model.</returns>
        internal static ParticipantSurvey ReaderParticipantSurvey(IDataReader reader, int[] idx)
        {
            var index = 0;
            return new ParticipantSurvey
            {
                Id = GetLongValue(reader, idx[index++]),
                ParticipantId = GetLongValue(reader, idx[index++]),
                SurveyId = GetLongValue(reader, idx[index++]),
                CurrentPrototypeTestId = GetLongValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read ParticipantSurvey model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The ParticipantSurvey model.</returns>
        internal static ParticipantSurvey ReaderParticipantSurvey(IDataReader reader)
        {
            return ReaderEntity(reader, ParticipantSurveyColumnNames, ReaderParticipantSurvey);
        }

        /// <summary>
        /// Helper method read ParticipantSurvey model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The ParticipantSurvey model list.</returns>
        internal static IList<ParticipantSurvey> ReaderParticipantSurveyList(IDataReader reader)
        {

            return ReaderEntityList(reader, ParticipantSurveyColumnNames, ReaderParticipantSurvey);
        }

        /// <summary>
        /// Helper method read PrototypeTest model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        /// <param name="idx">
        /// The column idx.
        /// </param>
        ///<returns>The PrototypeTest model.</returns>
        internal static PrototypeTest ReaderPrototypeTests(IDataReader reader, int[] idx)
        {
            var index = 0;
            return new PrototypeTest
            {
                Id = GetLongValue(reader, idx[index++]),
                PrototypeCode = GetStringValue(reader, idx[index++]),
                Iteration = GetIntValue(reader, idx[index++]),
                BeforeWeight = GetDoubleValue(reader, idx[index++]),
                AfterWeight = GetNullableDoubleValue(reader, idx[index++]),
                BeforePhotoUri = GetStringValue(reader, idx[index++]),
                AfterPhotoUri = GetStringValue(reader, idx[index++]),
                BeforePhotoLocalPath = GetStringValue(reader, idx[index++]),
                AfterPhotoLocalPath = GetStringValue(reader, idx[index++]),
                Completed = GetBoolValue(reader, idx[index])
            };
        }

        /// <summary>
        /// Helper method read PrototypeTest model.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The PrototypeTest model.</returns>
        internal static PrototypeTest ReaderPrototypeTests(IDataReader reader)
        {
            return ReaderEntity(reader, PrototypeTestColumnNames, ReaderPrototypeTests);
        }

        /// <summary>
        /// Helper method read PrototypeTest model list.
        /// </summary>
        ///
        /// <remarks>
        /// The internal exception may be thrown directly.
        /// </remarks>
        ///
        /// <param name="reader">
        /// The database reader.
        /// </param>
        ///<returns>The PrototypeTest model list.</returns>
        internal static IList<PrototypeTest> ReaderPrototypeTestsList(IDataReader reader)
        {
            return ReaderEntityList(reader, PrototypeTestColumnNames, ReaderPrototypeTests);
        }

        /// <summary>
        /// Builds the parameter for creating answer procedure based on Answer type.
        /// </summary>
        /// <param name="answer">The answer.</param>
        /// <returns>The parameters.</returns>
        internal static IDictionary<string, object> BuildAnswerParameter(Answer answer)
        {
            var parameters = new Dictionary<string, object>
            {
                {"AnswerType", answer.AnswerType.ToString()},
                {"QuestionId", answer.QuestionId},
                {"Value", null},
                {"AnswerOptionId", null},
                {"Input", null}
            };

            if (answer.AnswerType == AnswerType.RangeAnswer)
            {
                var rangeAnswer = answer as RangeAnswer;
                if (rangeAnswer != null)
                {
                    parameters["Value"] = rangeAnswer.Value;
                }
            }

            else if (answer.AnswerType == AnswerType.SingleAnswer)
            {
                var singleAnswer = answer as SingleAnswer;
                parameters["AnswerOptionId"] = ((SingleAnswer)answer).AnswerOptionId;
            }
            else
            {
                var singleInputAnswer = answer as SingleInputAnswer;
                if (singleInputAnswer != null)
                {
                    parameters["AnswerOptionId"] = singleInputAnswer.AnswerOptionId;
                    parameters["Input"] = singleInputAnswer.Input;

                }
            }
            return parameters;
        }
        /// <summary>
        /// Gets the answer options by question id.
        /// </summary>
        /// <param name="conn">The opened connection to the database.</param>
        /// <param name="spName">The stored procedure name.</param>
        /// <param name="questionId">The question id.</param>
        /// <returns>The answer option list.</returns>
        /// <exception>Any exception will be propagated to caller.</exception>
        internal static IList<AnswerOption> GetAnswerOptionsByQuestionId(IDbConnection conn, string spName
            , long questionId)
        {
            return ExecuteRetrieveSp(conn, spName,
                  ReaderAnswerOptions, new Dictionary<string, object>
                  {
                      {"QuestionId", questionId}
                  });
        }

        /// <summary>
        /// Gets the temp file name.
        /// </summary>
        /// <returns>The file name.</returns>
        internal static string GetTempFilePathWithExtension(string extension)
        {
            var path = Path.GetTempPath();
            var fileName = Guid.NewGuid().ToString() + extension;
            return Path.Combine(path, fileName);
        }

        /// <summary>
        /// Validates the participant entity.
        /// </summary>
        /// <param name="participant">The participant entity.</param>
        internal static void ValidateParticipant(Participant participant)
        {
            CheckNotNull(participant, "participant");
            CheckNotNullOrEmpty(participant.Username, "participant.Username");
            CheckPositive(participant.Weight, "participant.Weight");
            CheckPositive(participant.HeightFeet, "participant.HeightFeet");
            if (participant.ProfileAnswers != null)
            {
                foreach (var answer in participant.ProfileAnswers)
                {
                    ValidateAnswer(answer);
                }
            }
        }

        /// <summary>
        /// Validates the answer entity.
        /// </summary>
        /// <param name="answer">The answer entity.</param>
        internal static void ValidateAnswer(Answer answer)
        {
            CheckPropertyNotNull(answer, "answer");
            CheckPositive(answer.QuestionId, "answer.QuestionId");
            CheckPropertyNotNull(answer.AnswerType, "answer.AnswerType");
            if (AnswerType.SingleAnswer.Equals(answer.AnswerType))
            {
                CheckPositive(((SingleAnswer)answer).AnswerOptionId, "answer.AnswerOptionId");
            }
            else if (AnswerType.RangeAnswer.Equals(answer.AnswerType))
            {
                CheckPositive(((RangeAnswer)answer).Value, "answer.AnswerOptionId");
            }
            else if (AnswerType.SingleAnswerInput.Equals(answer.AnswerType))
            {
                CheckPositive(((SingleInputAnswer)answer).AnswerOptionId, "answer.AnswerOptionId");
                CheckPropertyNotNullOrEmpty(((SingleInputAnswer)answer).Input, "answer.Input");
            }
        }

        /// <summary>
        /// Validates the survey entity.
        /// </summary>
        /// <param name="survey">The survey.</param>
        /// <param name="isCreateOperation">If true checks only properties required for creation of survey.</param>
        internal static void ValidateSurveyEntity(Survey survey, bool isCreateOperation = false)
        {
            CheckNotNull(survey, "survey");
            CheckPropertyNotNullOrEmpty(survey.CellName, "survey.CellName");
            CheckPropertyNotNullOrEmpty(survey.Name, "survey.Name");
            CheckLegnth(64, survey.CellName, "survey.CellName");
            CheckLegnth(64, survey.Name, "survey.Name");
            CheckPositive(survey.ParticipantsNumber, "survey.ParticipantsNumber");
            CheckPropertyNotNull(survey.Status, "survey.Status");
            CheckPositive(survey.CompletedSurveysNumber, "survey.CompletedSurveysNumber", true);
            CheckPropertyNotNull(survey.Draft, "survey.Draft");
            if (survey.PrototypeCodes != null)
            {
                foreach (var code in survey.PrototypeCodes)
                {
                    ValidatePrototypeCodeEntity(code);
                }
            }
            if (survey.PostTestingQuestions != null)
            {
                foreach (var questions in survey.PostTestingQuestions)
                {
                    if (!isCreateOperation)
                    {
                        ValidateQuestionEntity(questions.Question);
                    }
                    else
                    {
                        CheckPositive(questions.Question.Id, "questions.Question.Id");
                    }
                }
            }
            if (survey.PrototypeQuestions != null)
            {
                foreach (var questions in survey.PrototypeQuestions)
                {
                    if (!isCreateOperation)
                    {
                        ValidateQuestionEntity(questions.Question);
                    }
                    else
                    {
                        CheckPositive(questions.Question.Id, "questions.Question.Id");
                    }
                }
            }

        }

        /// <summary>
        /// Validates the survey entity.
        /// </summary>
        /// <param name="prototypeCode">The prototype code.</param>
        internal static void ValidatePrototypeCodeEntity(PrototypeCode prototypeCode)
        {
            CheckNotNull(prototypeCode, "prototypeCode");
            CheckPropertyNotNullOrEmpty(prototypeCode.Code, "prototypeCode.Code");
            CheckLegnth(64, prototypeCode.Code, "prototypeCode.Code");
            CheckPositive(prototypeCode.PrototypesPerCode, "prototypeCode.PrototypesPerCode");
        }

        /// <summary>
        /// Validates the question entity.
        /// </summary>
        /// <param name="question">The question.</param>
        internal static void ValidateQuestionEntity(Question question)
        {
            CheckNotNull(question, "question");
            CheckPropertyNotNullOrEmpty(question.Text, "question.Text");
            CheckLegnth(1024, question.Text, "question.Text");
            CheckPropertyNotNull(question.QuestionType, "question.QuestionType");
            CheckPropertyNotNull(question.Conditional, "question.Conditional");
            if (question.Conditional)
            {
                if (question.Condition == null)
                {
                    throw new ArgumentException("Question condition cannot be null when question is conditional");
                }
                CheckPositive(question.Condition.ConditionalOnAnswerId, "question.Condition.ConditionalOnAnswerId");
                CheckPositive(question.Condition.ConditionalOnQuestionId,
                    "question.Condition.ConditionalOnQuestionId");
            }
            if (question.AnswerOptions != null)
            {
                foreach (var answerOption in question.AnswerOptions)
                {
                    ValidateAnswerOption(answerOption);
                }
            }
        }
        /// <summary>
        /// Validates the answer option entity.
        /// </summary>
        /// <param name="answerOption">the answer option entity.</param>
        internal static void ValidateAnswerOption(AnswerOption answerOption)
        {
            CheckPropertyNotNull(answerOption, "answerOption");
            CheckPositive(answerOption.QuestionId, "answerOption.QuestionId");
            CheckPropertyNotNull(answerOption.Type, "answerOption.Type");
            if (AnswerType.SingleAnswer.Equals(answerOption.Type))
            {
                CheckPositive(((SingleAnswerOption)answerOption).Value, "answerOption.Value");
                CheckPropertyNotNullOrEmpty(((SingleAnswerOption)answerOption).Label, "answerOption.Label");
                CheckLegnth(64, ((SingleAnswerOption)answerOption).Label, "answerOption.Label");
            }
            else if (AnswerType.RangeAnswer.Equals(answerOption.Type))
            {
                CheckPositive(((RangeAnswerOption)answerOption).FromValue, "answerOption.FromValue", true);
                CheckPositive(((RangeAnswerOption)answerOption).ToValue, "answerOption.ToValue", true);
                CheckPropertyNotNullOrEmpty(((RangeAnswerOption)answerOption).FromValueLabel,
                    "answerOption.FromValueLabel");
                CheckLegnth(64, ((RangeAnswerOption)answerOption).FromValueLabel, "answerOption.FromValueLabel");
                CheckPropertyNotNullOrEmpty(((RangeAnswerOption)answerOption).ToValueLabel,
                   "answerOption.ToValueLabel");
                CheckLegnth(64, ((RangeAnswerOption)answerOption).ToValueLabel, "answerOption.ToValueLabel");
                if (((RangeAnswerOption)answerOption).Increment != null)
                {
                    CheckPositive(Convert.ToInt32(((RangeAnswerOption)answerOption).Increment),
                        "answeroption.Increment");
                }
            }
            else if (AnswerType.SingleAnswerInput.Equals(answerOption.Type))
            {
                CheckPositive(((SingleAnswerInputOption)answerOption).Value, "answerOption.Value");
                CheckPropertyNotNullOrEmpty(((SingleAnswerInputOption)answerOption).Label, "answerOption.Label");
                CheckLegnth(64, ((SingleAnswerInputOption)answerOption).Label, "answerOption.Label");
            }
        }

        /// <summary>
        /// validates the prototype test.
        /// </summary>
        /// <param name="prototypeTest">The prototype test.</param>
        internal static void ValidatePrototypeTest(PrototypeTest prototypeTest)
        {
            CheckPropertyNotNullOrEmpty(prototypeTest.PrototypeCode, "prototypeTest.PrototypeCode");
            CheckLegnth(64, prototypeTest.PrototypeCode, "prototypeTest.PrototypeCode");
            CheckPositive(prototypeTest.Iteration, "prototypeTest.Iteration");
            CheckPositive(prototypeTest.BeforeWeight, "prototypeTest.BeforeWeight");
            if (prototypeTest.Answers != null)
            {
                foreach (var answer in prototypeTest.Answers)
                {
                    ValidateAnswer(answer);
                }
            }
        }

        /// <summary>
        /// Validates the file extension 
        /// </summary>
        /// <param name="name">The file name.</param>
        /// <param name="fileName">The string name.</param>
        public static void ValidateFileNameWithExtension(string name, string fileName)
        {
            CheckNotNullOrEmpty(name, fileName);

            if (string.IsNullOrEmpty(name) || name.IndexOf('.') < 0 ||
                name.Substring(name.LastIndexOf('.')).Length < 1)
            {
                throw new ArgumentException(
                    "File name is invalid, filename must contain extension. For example abc.jpg");
            }
        }


        /// <summary>
        /// Label and sort questions
        /// </summary>
        /// <param name="questions">Questions list</param>
        /// <returns>Labled and sorted questions.</returns>
        internal static IList<Question> LabelAndSort(IList<Question> questions)
        {
            foreach (var question in questions)
            {
                Question localQuestion = question;
                string questionId = Convert.ToString(question.Id);
                string sortLabel = questionId;

                while (true)
                {
                    QuestionCondition condition = localQuestion.Condition;

                    if (condition != null)
                    {
                        long parentId = condition.ConditionalOnQuestionId;

                        sortLabel = Convert.ToString(parentId) + '_' + sortLabel;

                        bool found = false;
                        for (var questionIdx = 0; questionIdx < questions.Count; ++questionIdx)
                        {
                            Question innerQuestion = questions[questionIdx];
                            long innerQuesetionId = innerQuestion.Id;

                            if (innerQuesetionId == parentId)
                            {
                                localQuestion = innerQuestion;
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                            break;
                        }
                    }
                    else
                    {
                        // no parent anymore.
                        break;
                    }

                }

                sortLabel = '/' + sortLabel;

                question.RelationshipLabel = sortLabel;
            }

            questions = questions.OrderBy(item => item.RelationshipLabel).ToList();
            return questions;
        }
    }
}
