/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Tholos.Entities;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the participant related operations.
    /// </para>
    /// </summary>
    /// 
    /// <remarks>
    /// This interface has <see cref="ServiceContractAttribute"/> attribute and all its methods are exposed
    /// with <c>OperationContract</c>, <c>TransactionFlow(TransactionFlowOption.Allowed)</c> and
    /// <c>FaultContract(typeof(ServiceFaultDetail))</c> attributes.
    /// </remarks>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    [ServiceKnownType(typeof(RangeAnswer))]
    [ServiceKnownType(typeof(SingleAnswer))]
    [ServiceKnownType(typeof(SingleInputAnswer))]
    public interface IParticipantService
    {
        /// <summary>
        /// Creates the given participant, and returns the ID of the created entity.
        /// </summary>
        ///
        /// <param name="participant">The participant to create.</param>
        ///
        /// <returns>The Id of the created participant.</returns>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="participant"/> is null.</item>
        /// <item><see cref="EntityNotFoundException"/> If entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/Participants"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        long CreateParticipant(Participant participant);

        /// <summary>
        /// Gets the participant by the given Id.
        /// </summary>
        ///
        /// <param name="participantId">The Id of the participant to retrieve.</param>
        ///
        /// <returns>The participant corresponding to the given Id,
        /// or <c>null</c> if none found.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantId"/> is <c>null</c>.</item>
        /// <item>
        /// <see cref="ArgumentException"/> If <paramref name="participantId"/> is not valid positive number.
        /// </item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/Participants/{participantId}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Participant GetParticipantById(string participantId);

        /// <summary>
        /// Gets the participant by the given username.
        /// </summary>
        ///
        /// <param name="username">The username of the participant to retrieve.</param>
        ///
        /// <returns>The participant corresponding to the given username,
        /// or <c>null</c> if none found.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="username"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="username"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/Participants/params?username={username}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Participant GetParticipantByUserName(string username);

        /// <summary>
        /// Gets the profile questions fully populated with the answer options.
        /// </summary>
        ///
        /// <returns>The profile questions.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/ProfileQuestions",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<Question> GetAllProfileQuestions();
    }
}

