/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.Unity;
using MySql.Data.MySqlClient;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This is the base class for all service implementations that access database persistence.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// <para>
    /// This class is mmutable but effectively thread-safe.
    /// </para>
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public abstract class BasePersistenceService : BaseService
    {
        /// <summary>
        /// <para>
        /// Gets or sets the connection string name.
        /// </para>
        /// </summary>
        /// <value>
        /// The connection string name. It is not null and not empty after injection through the Unity.
        /// </value>
        [Dependency("ConnectionStringName")]
        public string ConnectionStringName
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Gets or sets the database type name.
        /// </para>
        /// </summary>
        /// <value>
        /// The database type name. It is not null after injection through the Unity.
        /// </value>
        [Dependency]
        public string DatabaseTypeName
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="BasePersistenceService"/> class.
        /// </para>
        /// </summary>
        public BasePersistenceService()
        {
        }

        /// <summary>
        /// <para>
        /// Gets <see cref="IDbConnection"/> instance to access the persistence.
        /// </para>
        /// </summary>
        ///
        /// <returns>
        /// The created connection in opened state.
        /// </returns>
        ///
        /// <exception cref="ConfigurationException">
        /// If the connection string with name <see cref="ConnectionStringName"/> is missing or has invalid format.
        /// </exception>
        /// <exception cref="PersistenceException">
        /// If error occurs while obtaining the connection.
        /// </exception>
        protected IDbConnection GetConnection()
        {
            IDbConnection connection = null;
            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                if (settings == null)
                {
                    throw new ConfigurationException(
                        string.Format("Connection string with name {0} is missing.", ConnectionStringName));
                }

                DatabaseType databaseType =
                    (DatabaseType)
                        Enum.Parse(typeof(DatabaseType), ConfigurationManager.AppSettings[DatabaseTypeName], true);

                switch (databaseType)
                {
                    case DatabaseType.SqlServer:
                        connection = new SqlConnection(settings.ConnectionString);
                        break;
                    case DatabaseType.MySql:
                        connection = new MySqlConnection(settings.ConnectionString);
                        break;
                }
                // Create and open connection
                if (connection != null)
                {
                    connection.Open();
                }
                return connection;
            }
            catch (ConfigurationException)
            {
                throw;
            }
            catch (ArgumentException ex)
            {
                throw new ConfigurationException("Connection string is invalid.", ex);
            }
            catch (Exception ex)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }

                throw new PersistenceException("Error occurred, while obtaining database connection.", ex);
            }

        }

        /// <summary>
        /// <para>
        /// Closes the <see cref="IDbConnection"/> connection.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ArgumentNullException">
        /// If connection is null
        /// </exception>
        protected void CloseConnection(IDbConnection connection)
        {
            Helper.CheckNotNull(connection, "connection");
            connection.Close();
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If there are required injection fields that are not injected.
        /// </exception>
        public override void CheckConfiguration()
        {
            base.CheckConfiguration();
            Helper.CheckConfiguration(ConnectionStringName, "ConnectionString");
            Helper.CheckConfiguration(DatabaseTypeName, "DatabaseTypeName");
        }
    }
}

