/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*
*/

using System;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This class is the realization of the <see cref="ISecurityService"/> service contract.
    /// It get roles for the username.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Changed the stored procedure name.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class SecurityService : BasePersistenceService, ISecurityService
    {

        /// <summary>
        /// Represents the name of the stored procedure to retrieve participant roles by Username.
        /// </summary>
        private const string GetParticipantRolesSpName = "spGetParticipantRoles";


        /// <summary>
        /// Represents the name of the stored procedure to retrieve user roles by Username.
        /// </summary>
        private const string GetUserRoleNamesSpName = "spGetUserRoleNames";

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="SecurityService"/> class.
        /// </para>
        /// </summary>
        public SecurityService()
        {
        }

        /// <summary>
        /// Get roles. It will check for both - Manager and Participant.
        /// </summary>
        ///
        /// <param name="username">The username.</param>
        ///
        /// <returns>The roles.</returns>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="username"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="username"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        public IList<string> GetRoles(string username)
        {
            return Helper.LoggingWrapper(Logger,
                () =>
                {
                    Helper.CheckNotNullOrEmpty(username, "username");
                    var parameters = new Dictionary<string, object>
                    {
                        {"Username", username}
                    };

                    using (var conn = GetConnection())
                    {
                        var participantRoles = Helper.ExecuteRetrieveSp(conn, GetParticipantRolesSpName,
                            reader => Helper.ReadStringList(reader), parameters);

                        if (participantRoles.Count > 0)
                        {
                            return participantRoles;
                        }

                        var userRoles = Helper.ExecuteRetrieveSp(conn, GetUserRoleNamesSpName,
                            reader => Helper.ReadStringList(reader), parameters);

                        return userRoles.Count > 0 ? userRoles : new List<string>();
                    }
                }, username);
        }
    }
}

